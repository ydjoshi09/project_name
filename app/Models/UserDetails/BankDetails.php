<?php

namespace App\Models\UserDetails;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;

class BankDetails extends Model
{
    //Author : Sheevathsa
   	//Date: 7/4/2018
    use SoftDeletes;
	protected $table='bank_details';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'user_id',
		'bank_name',
		'ifsc_code',
		'micr_code',
		'branch_name',
		'branch_email',
		'city_id',
		'address',
		'account_holders_name',
		'account_type',
		'account_no',
		'cancelled_cheque_attachment',
		'account_verified',
		'active_flag',
		'remark'
	];

	// This function will create a new entry to bank details table.
	public function store($request)
	{
		$bank_details=$this->create($request);    
		return $bank_details;
	}

	//This function returns entry from bank details table.
	public function getBankDetailsById($id)
	{
		return $this->where('id',$id)->first();
	}

	//This function will update the details of entry with new values.
	public function updateBankDetails($id,$request)
	{


		$bank_details = new BankDetails();
		$bank_details=$bank_details->getBankDetailsById($id);
		$bank_details->update($request);
		return 1;
	}

	// This will set provided id account as active account for user.That is active flag is set to 1,
	// If any other bank account is currently active then its active flag will be set to 0.
	public function updateActiveBankDetails($id,$user_id)
	{
		$this->where('user_id',$user_id)->update(['active_flag'=>0]);
		$this->where('id',$id)->update(['active_flag'=>1]);
		return 1;
	}

	// This will return active bank account of user.
	public function getActiveBankDetails($user_id)
	{
		return $this->where('user_id',$user_id)->where('active_flag','=',1)->first();
	}


	// This will return list of bank accounts of user.
	public function getBankDetails($user_id)
	{
		// return $this->where('user_id',$user_id)->lists('bank_name', 'id');

		return $this->where('id',$user_id)->get();
	}

	  //getBankInformation($user_id) - this function will return all details from bank details table,countries,states,cities table where user_id is $user_id 

	 public function getBankInformation($user_id)
    {
       
       $bank_details=DB::table('bank_details as b')
       ->join('cities AS c','c.id','=','b.city_id')
       ->join('states AS s','s.id','=','c.state_id')
       ->join('countries AS co','co.id','=','s.country_id')
       ->select('b.*','co.id as country_id','co.country_name','co.phone_code','s.id as state_id','s.state_name','c.id as city_id','c.city_name')
       ->where('b.user_id',$user_id)
       ->first();
   

         return $bank_details;

    }

     //getStatutoryDetails($user_id) - this function will return all details from bank details table,countries,states,cities table where user_id is $user_id 

     public function getUserBankDetails($user_id)
    {
       
       $bank_details=DB::table('bank_details as b')
       ->join('cities AS c','c.id','=','b.city_id')
       ->join('states AS s','s.id','=','c.state_id')
       ->join('countries AS co','co.id','=','s.country_id')
       ->select('b.*','co.id as country_id','co.country_name','co.phone_code','s.id as state_id','s.state_name','c.id as city_id','c.city_name')
       ->where('b.user_id',$user_id)
       ->get();
   

         return $bank_details;

    }
    
}