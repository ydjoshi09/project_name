<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">

    <title>
     Agri Trade
 </title>

 <meta name="keywords" content="">


 <!-- styles -->
 <link href="obaju/obaju/css/font-awesome.css" rel="stylesheet">
 <link href="obaju/obaju/css/bootstrap.min.css" rel="stylesheet">
 <link href="obaju/obaju/css/animate.min.css" rel="stylesheet">
 <link href="obaju/obaju/css/owl.carousel.css" rel="stylesheet">
 <link href="obaju/obaju/css/owl.theme.css" rel="stylesheet">

 <!-- theme stylesheet -->
 <link href="obaju/obaju/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

 <!-- your stylesheet with modifications -->
 <link href="obaju/obaju/css/custom.css" rel="stylesheet">



 <script src="obaju/obaju/js/respond.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

 <!-- jQuery 2.1.3 -->
 <script src="{{ asset ("/agritrade/tinymce/js/tinymce/tinymce.min.js") }}"></script>
 <script src="{{ asset ("/agritrade/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
 <!-- Bootstrap 3.3.2 JS -->
 <script src="{{ asset ("/agritrade/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
 <script src="{{ asset ("/agritrade/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
 <script src="{{ asset ("/agritrade/admin-lte/plugins/fastclick/fastclick.min.js") }}"></script>
 <script src="{{ asset ("/agritrade/admin-lte/plugins/notify/bootstrap-notify.min.js") }}"></script>
 <script src="{{ asset ("/agritrade/js/jquery.validate.js") }}"></script>

 <!-- AdminLTE App -->
 <script src="{{ asset ("/agritrade/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
 <script src="{{ asset ("/agritrade/js/defaults.js") }}" type="text/javascript"></script>
 <script src="{{ asset ("/agritrade/js/multifilter.js") }}" type="text/javascript"></script>
 <script src="{{ asset ("/agritrade/js/printThis.js") }}" type="text/javascript"></script>
 <script src="{{ asset ("/agritrade/js/moment.js") }}" type="text/javascript"></script>
 <script type="text/javascript">
  $(function(){
   $('[data-toggle="popover"]').popover();
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('agritrade.layouts.header')
        <div class="spacer">
            &nbsp;
        </div>
        <div class="clearfix"></div>
        <div class="col-md-2 nomargin">

           @include('agritrade.layouts.sidebar')

       </div>
       <div class="col-md-10 page_hight" >


        @yield('content')

    </div>


    <!-- Footer -->
    @include('agritrade.layouts.footer')

</div>
@section('script')
<!-- REQUIRED JS SCRIPTS -->



@show

</body>
</html>