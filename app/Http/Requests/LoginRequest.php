<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {


        if ($this->method() == 'PUT')
        {
            // Update operation, exclude the record with id from the validation:
            $login_rule1 = 'required:user_details,email,' . $this->get('id');
            $login_rule2 = 'required:user_details,password,' . $this->get('id');

            



        }
        else
        {
            // Create operation. There is no id yet.
            $login_rule1 = 'required:user_details,email';
            $login_rule2 = 'required:user_details,password';


        }
        
    
        return [
            'user_name'=>$login_rule1,
            'password'=>$login_rule2,

            

           
        ];
    }
}
