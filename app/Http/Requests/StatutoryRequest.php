<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StatutoryRequest extends Request {

	public function authorize() {
		return true;
	}

	public function rules() {

		return [
			'firm_name' => 'required',
			'pan_no' => 'required|min:10|max:10',
			'gst_no' => 'required|min:15|max:15',
			'type_of_firm' => 'required|not_in:0',
			'city_id' => 'required|not_in:0',

			'address' => 'required',
			'pan_image_file' => 'required',
			'gst_image_file' => 'required',

		];
	}
}
