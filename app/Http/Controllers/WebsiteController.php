<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\UserDetails\Users;

class WebsiteController extends Controller {

	public function index() {
		return view('agritrade.website.index');
	}

	public function aboutUs() {
		return view('agritrade.website.about');
	}

	public function products() {
		return view('agritrade.website.products');
	}

	public function contactUs() {
		return view('agritrade.website.contact_us');
	}

	public function nationalDistributorship() {
		return view('agritrade.website.national_distributorship');
	}

	public function billCollection() {
		return view('agritrade.website.bill_collection');
	}

	public function importExport() {
		return view('agritrade.website.import_export');
	}

	public function register() {
		return view('agritrade.website.register');
	}

	public function statutory() {
		return view('agritrade.website.registration.statutorydetails.create');
	}
	public function bankDetails() {
		return view('agritrade.website.registration.bankdetails.create');
	}

	public function editBasicInfo($id) {
		$user = new Users;
		$user = $user->getTraderById($id);
		return view('agritrade.website.registration.basic_info.edit')->with('user', $user);

	}

	public function login() {
		return view('agritrade.website.login');
	}

}
