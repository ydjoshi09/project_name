<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SubCategory extends Model
{
    //
    use SoftDeletes;
	protected $table='sub_categories';
	protected $dates = ['deleted_at'];
	protected $fillable = [
			'category_id',
            'sub_category_name',
            'description',
            'image',
            'our_margin',
            'created_by',
            'updated_by'
	];

    public function getAllSubCategories($category_id)
    {
       
        $sub_categories=DB::table('sub_categories AS sc')
         ->join('categories AS c','sc.category_id','=','c.id')
         ->where('sc.category_id',$category_id)
         ->select('sc.*','c.category_name')
        ->get();

         return $sub_categories;

    }
}
