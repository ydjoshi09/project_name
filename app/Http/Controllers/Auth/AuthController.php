<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Input;
use Redirect;
use Request;
use Session;
use Validator;

class AuthController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Registration & Login Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles the registration of new users, as well as the
		    | authentication of existing users. By default, this controller uses
		    | a simple trait to add these behaviors. Why don't you explore it?
		    |
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data) {
		return Validator::make($data, [

			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data) {
		return User::create([

			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}

	// protected function showLogin()
	// {
	//    $users=DB::table('users as u')
	//    ->select('u.*')
	//    ->whereNull('u.deleted_at')
	//    ->orderBy('u.first_name')
	//    ->get();

	//     return view('auth.login')->with('users',$users);
	// }

	public function doLogins(Request $request) {
		// dd(Input::all());
		// validate the info, create rules for the inputs
		$rules = array(
			'email' => 'required', // make sure the email is an actual email
			'password' => 'required|min:3', // password can only be alphanumeric and has to be greater than 3 characters

		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {

			return 0;
		} else {

			// create our user data for the authentication
			$userdata = array(
				'email' => Input::get('email'),
				'password' => Input::get('password'),
			);

// dd(Auth::attempt($userdata));
			// attempt to do the login
			if (Auth::attempt($userdata)) {

				return 1;
			} else {

				return 0;
			}

		}
	}

	public function signOut() {

		Auth::logout();
		Session::flush();
		return Redirect::to('/')->with('message', 'Successfully Logged Out')->with('er_type', 'success');
	}
}
