<?php

namespace App\Models\Trading;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model {
	//
	use SoftDeletes;
	protected $table = 'purchase_order';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'buyer_id',
		'po_date',
		'billing_address',
		'shipping_address',
		'billing_address',
		'bank_details',
		'reference_document',
		'remarks',
		'po_number',
		'status',
	];

	// This function will create a po entry.
	public function store($request) {
		$po = $this->where('buyer_id', $request->buyer_id)->orderBy('created_at', 'Desc')->first();

		if ($po->po_number) {
			$request['po_number'] = $po->po_number + 1;
		} else {
			$request['po_number'] = 1;
		}

		return $this->create($request->all());
	}

	// This will return particular po entry.
	public function getPurchaseOrderById($id) {
		return $this->where('id', $id)->first();
	}

	// update_status(1,1):confirm po;
	// update status(1,2): payment done.
	public function updatePOStatus($id, $no) {
		$purchase_order = new PurchaseOrder();
		$purchase_order = $purchase_order->getPurchaseOrderById($id);
		$purchase_order->status = $no;
		$purchase_order->save();
	}

	// this will list all the purchase orders
	public function getPurchaseOrders($status, $user_id) {
		return $this->where('buyer_id', $user_id)->whereIn('status', $status)->get();
	}

	public function getPODetails($id) {

		$item_details = DB::table('cart_items as c')
			->join('items as i', 'i.id', '=', 'c.item_id')
			->select('i.item_name')
			->where('c.po_id', $id)
			->get();

		return $item_details;
	}

	public function getShipmentPODetails($id, $shipment_flag) {

		$item_details = DB::table('cart_items as c')
			->join('items as i', 'i.id', '=', 'c.item_id')
			->select('i.item_name', 'c.id as cart_id', 'i.id as item_id', 'c.shipment_flag')
			->where('c.po_id', $ac->id)
			->where('c.shipment_flag', '=', $shipment_flag)
			->get();

		return $item_details;
	}

	public function deletePO($id) {
		return DB::table('purchase_order')->where('id', $id)->delete();
	}

	public function getPODeatils($status, $user_id) {
		$purchase_order = DB::table('purchase_order AS po')
			->join('billing_address AS ba', 'po.billing_address', '=', 'ba.id')
			->join('shipping_address AS sa', 'po.shipping_address', '=', 'sa.id')
			->join('bank_details AS bd', 'po.bank_details', '=', 'bd.id')
			->join('statutory_details AS sd', 'po.buyer_id', '=', 'sd.user_id')
			->where('po.buyer_id', $user_id)
			->where('po.status', $status)
			->select('po.*', 'ba.address as bill_address', 'sa.address as shipp_address', 'sd.firm_name', 'sd.address', 'bd.bank_name', 'bd.branch_name', 'bd.account_no', 'bd.branch_email')
			->get();

		return $purchase_order;
	}

	public function getAllPODetailsById($id) {
		$purchase_order = DB::table('purchase_order AS po')
			->join('billing_address AS ba', 'po.billing_address', '=', 'ba.id')
			->join('shipping_address AS sa', 'po.shipping_address', '=', 'sa.id')
			->join('bank_details AS bd', 'po.bank_details', '=', 'bd.id')
			->join('statutory_details AS sd', 'po.buyer_id', '=', 'sd.user_id')
			->where('po.id', $id)
			->select('po.*', 'ba.address as bill_address', 'sa.address as shipp_address', 'sd.firm_name', 'sd.address', 'bd.bank_name', 'bd.branch_name', 'bd.account_no', 'bd.branch_email')
			->first();

		return $purchase_order;
	}

	public function updatePdfAttachment($id, $pdf_path) {
		$purchase_order = new PurchaseOrder();
		$purchase_order = $purchase_order->getPurchaseOrderById($id);
		$purchase_order->reference_document = $pdf_path;
		$purchase_order->status = 1;
		$purchase_order->save();
	}

}
