<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;

class FileUpload extends Controller {
	public function upload(Request $request) {

		if (isset($request->image_file)) {

			$file = Input::file('image_file');

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = storage_path() . '/app/' . $request->store_folder;
				$file->move($file_path, $file_name);
				return $file_name;
			} else {
				return 0;
			}

		} elseif (isset($request->pan_image_file)) {

			$file = Input::file('pan_image_file');

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG" || $extension == "pdf") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = storage_path() . '/app/' . $request->store_folder;
				$file->move($file_path, $file_name);

				return $file_name;
			} else {
				return 0;
			}

		} elseif (isset($request->gst_image_file)) {

			$file = Input::file('gst_image_file');

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG" || $extension == "pdf") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = storage_path() . '/app/' . $request->store_folder;
				$file->move($file_path, $file_name);

				return $file_name;
			} else {
				return 0;
			}

		} elseif (isset($request->item_image_file)) {

			$file = Input::file('item_image_file');

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG" || $extension == "pdf") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = 'agritrade/images/' . $request->store_folder;
				$file->move($file_path, $file_name);

				return $file_name;
			} else {
				return 0;
			}

		} elseif (isset($request->parameter_id)) {

			$file = Input::file('parameter_' . $request->parameter_id);

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG" || $extension == "pdf") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = 'agritrade/images/' . $request->store_folder;
				$file->move($file_path, $file_name);

				return $file_name;
			} else {
				return 0;
			}

		} elseif (isset($request->revise_parameter_id)) {

			$file = Input::file('revise_parameter_' . $request->revise_parameter_id);

			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

			if ($extension == "png" || $extension == "jpeg" || $extension == "jpg" || $extension == "JPG" || $extension == "pdf") {
				$file_name = (time() + rand(0, 1500000) - rand(0, 1500000) * 977) . '.' . "jpeg";
				$file_path = 'agritrade/images/' . $request->store_folder;
				$file->move($file_path, $file_name);

				return $file_name;
			} else {
				return 0;
			}

		}
	}

}
