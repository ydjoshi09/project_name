<?php

namespace App\Models\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierPayment extends Model
{
    //Author : Sheevathsa
    //Date: 7/4/2018
    use SoftDeletes;
    protected $table='supplier_payments';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'supplier_po_id',
        'payment_amount',
        'tax_amount',
        'quantity',
        'payment_status',
        'paid_amount',
        'transaction_id'
    ];

    // Create supplier payment entry
    public function store($request)
    {
        return $this->create($request);
    }
}
