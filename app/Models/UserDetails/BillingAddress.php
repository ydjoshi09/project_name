<?php

namespace App\Models\UserDetails;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;

class BillingAddress extends Model
{
    //Author : Sheevathsa
   	//Date: 6/4/2018
    use SoftDeletes;
	protected $table='billing_address';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'user_id',
		'city_id',
		'address',
		'active_flag',
	];

// This will create a new billing address.
	public function store($request)
	{

		
		$billing_address=$this->create($request);    
		return $billing_address;
	}

	public function getBillingAddressById($id)
	{
		return $this->where('id',$id)->first();
	}
	// This function will store new values to entry.
	public function updateBillingAddressDetails($id,$request)
	{
		$billing_address = new BillingAddress();
		$billing_address=$billing_address->getBillingAddressById($id);
		$billing_address->update($request);
		return 1;
	}

	// This function will set active_flag to 1, if any other entry which belongs to same user and has active flag 1,will bu updated to active flag 0
	public function updateActiveBillingAddressDetails($id,$user_id)
	{
		$this->where('user_id',$user_id)->update(['active_flag'=>0]);
		$this->where('id',$id)->update(['active_flag'=>1]);
		return 1;
	}

	
	// This will return Active Billing Address of user.
	public function getActiveBillingAddressDetail($id)
	{
		return $this->where('user_id',$id)->where('active_flag',1)->first();
	}

	// This will return the list of Billing address of user.
	public function getUserBillingAddressDetail($user_id)
	{
		// return $this->where('user_id',$user_id)->lists('address', 'id');

		$billing_address=DB::table('billing_address AS ba')
         ->join('cities AS c','ba.city_id','=','c.id')
         ->join('states AS s','c.state_id','=','s.id')
         ->join('countries AS co','s.country_id','=','co.id')
         ->where('ba.user_id',$user_id)
         ->select('ba.*','c.city_name','s.state_name','co.country_name','co.id as country_id','s.id as state_id')
        ->get();

        return $billing_address;
	}
}
