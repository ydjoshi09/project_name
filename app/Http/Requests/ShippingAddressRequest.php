<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ShippingAddressRequest extends Request {

	public function authorize() {
		return true;
	}

	public function rules() {

		if ($this->method() == 'PUT') {
			// Update operation, exclude the record with id from the validation:
			$shipping_rule1 = 'required:shipping_address,address,' . $this->get('id');

		} else {
			// Create operation. There is no id yet.
			$shipping_rule1 = 'required:shipping_address,address';

		}

		return [
			'address' => $shipping_rule1,

			'city_id' => 'required|not_in:0',

		];
	}
}
