<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	public function authorize() {
		return true;
	}

	public function rules() {

		if ($this->user_id != 0) {

			$email_rule = 'required|email|unique:users,email,' . $this->get('user_id');
			$password_rule = '';
			$confirm_password_rule = '';
		} else {

			$email_rule = 'required|email|unique:users,email';
			$password_rule = 'required|min:5|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!$#%@]).*$/';
			$confirm_password_rule = 'required|min:5|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!$#%@]).*$/';

		}

		return [
			'first_name' => 'required|alpha|min:2',
			'last_name' => 'required',
			'mobile_no_cc' => 'required|not_in:0',
			'mobile_no' => 'required|min:10|max:10',
			'whatsapp_no_cc' => 'required|not_in:0',
			'password' => $password_rule,
			'confirmation_password' => $confirm_password_rule,
			'whatsapp_no' => 'required|min:8|max:15',
			'email' => $email_rule,
			'profile_image' => 'required',

		];
	}
}
