<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parameters extends Model
{
    use SoftDeletes;
	protected $table='parameters';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'parameter_name',
            'parameter_type',
            'description',
            'created_by',
            'updated_by'
	];
}
