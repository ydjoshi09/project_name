


<div class="row">
    <div class="clearfix"></div>

      {!! Form::hidden('profile_image',null,['id'=>'profile_image'])!!}
    <div class="col-md-4 form-group" id="first_name_field">
    {!! Form::label('first_name', 'First Name *') !!}
    {!! Form::text('first_name',null,['class'=>'form-control ', 'placeholder' =>'Enter First Name','required'])!!}

        <small class="error" style="color:red" hidden="true" id="first_name_error"></small>
    </div>
    <div class="col-md-4 form-group" id="middle_name_field" >
       {!! Form::label('middle_name', 'Middle Name ') !!}
    {!! Form::text('middle_name',null,['class'=>'form-control', 'placeholder' =>'Enter Middle Name'])!!}
        <small class="error" style="color:red" hidden="true" id="middle_name_error"></small>
    </div>

    <div class="col-md-4 form-group" id="last_name_field">
     {!! Form::label('last_name', 'Last Name *') !!}
    {!! Form::text('last_name',null,['class'=>'form-control ', 'placeholder' =>'Enter Last Name','required'])!!}
        <small class="error" hidden="true" style="color:red" id="last_name_error"></small>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6 form-group" id="mobile_no_field">

         {!! Form::label('mobile_no', 'Mobile No *') !!}
        <div class="">
            <div class="col-md-3 " style="padding:0px;">


                <div class="form-group @if($errors->first('mobile_no_cc')) has-error @endif">
                    {!!Form::select('mobile_no_cc', array(),null, ['class' => 'form-control','id'=>'mobile_no_cc','name'=>'mobile_no_cc','notequal'=>'0','data-live-search'=>'true','required']) !!}
                </div>
            </div>

            <div class="col-md-9 " style="padding: 0px;">
                {!! Form::input('number','mobile_no', null, ['class'=>'form-control', 'placeholder' =>'Enter Mobile No','required'])!!}
            </div>
        </div>
        <small class="error" style="color:red" id="mobile_no_error" hidden="true"></small>

    </div>

    <div class="col-md-6 form-group" id="whatsapp_no_field">
          {!! Form::label('whatsapp_no', 'WhatsApp No *') !!}
        <div class="">
            <div class="col-md-3 " style="padding:0px;">


                <div class="form-group @if($errors->first('whatsapp_no_cc')) has-error @endif">
                    {!!Form::select('whatsapp_no_cc', array(),null, ['class' => 'form-control required','id'=>'whatsapp_no_cc','name'=>'whatsapp_no_cc','notequal'=>'0','data-live-search'=>'true']) !!}
                </div>

                <!-- /input-group -->
            </div>

            <div class="col-md-9 " style="padding: 0px;">
                {!! Form::input('number','whatsapp_no', null, ['class'=>'form-control', 'placeholder' =>'Enter WhatsApp No','required'])!!}
            </div>
        </div>
        <small class="error" style="color:red" id="whatsapp_no_error" hidden="true"></small>

    </div>
    <div class="clearfix"></div>

    <div class="col-md-4 form-group" id="email_field">

           {!! Form::label('email', 'Email Id *') !!}
    {!! Form::input('email','email',null,['class'=>'form-control ', 'placeholder' =>'Enter Email Id','required'])!!}

        <small class="error" style="color:red" id="email_error" hidden="true"></small>

    </div>
    <div class="col-md-4 form-group" id="password_field">
       {!! Form::label('password', 'Password *') !!}
    {!! Form::input('password','password',null,['class'=>'form-control ', 'placeholder' =>'Enter Password','required'])!!}
        <small class="error" style="color:red" id="password_error" hidden="true"></small>

    </div>
    <div class="col-md-4 form-group" id="confirmation_password_field">
       {!! Form::label('confirmation_password', 'Confirmation Password *') !!}
    {!! Form::input('password','confirmation_password',null,['class'=>'form-control ', 'placeholder' =>'Enter Confirmation Password','required'])!!}
        <small class="error" id="confirmation_password_error" style="color:red" hidden="true"></small>

    </div>




    <!-- Profile Image field -->
    <div class="col-md-6" id="profile_attachment_field">
        {!! Form::label('Profile Image*') !!}
         {!! Form::file('image_file',['class'=>'form-control ','id'=>'image_file','required'])!!}

        <small class="error" style="color:red" id="profile_attachment_error" hidden="true"></small>

    </div>

    <div>
        <img   id="img"   height="100" width="100">
    </div>

    <div class="clearfix"></div>

</div>

