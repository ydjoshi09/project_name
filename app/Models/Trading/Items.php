<?php

namespace App\Models\Trading;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;

class Items extends Model
{
    //
    use SoftDeletes;
	protected $table='items';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'item_code',
        'sub_category_id',
        'supplier_id',
        'id',
        'item_name',
        'item_image',
        'package_quantity',
        'unit_of_measure',
        'package_cost',
        'min_order_quantity',
        'max_order_quantity',
        'igst',
        'cgst',
        'sgst',
        'expires_at',
        'status',
        'reference_id',
        
        
       
	];

    public function store($request)
    {        
        $items=$this->create($request);    
        return $items;
    }



    // This function will return items which have expires_at > current time and status 1
	public function getActiveItems($user_id)
    {
        return $this->whereDate('expires_at', '>', Carbon::now())
        ->where('status',1)->where('supplier_id',$user_id)->get();
    }

    // This function will return the items which have expires_at < current time and status 1
    public function getExpiredItems($user_id)
    {
        return $this->whereDate('expires_at', '<', Carbon::now())->where('status',1)->where('supplier_id',$user_id)->get();
    }

    // This function will list the items which have status as 2
    public function getDiscardedItems($user_id)
    {
        return $this->where('status','=',2)->where('supplier_id',$user_id)->get();
    }



    // This function will return entry with it provided by user.
    public function reconsiderItem($id)
    {
      
        $this->where('id',$id)->update(['status'=>2]);
        return 1;
    }

    // This function will add new column to table with name as parameter_id and parameter name as comment.
    public function addParameter($request)
    {
        if($request->parameter_type==3)
        {
            DB::statement(DB::raw("ALTER TABLE `items` ADD `".$request->parameter_id."` DateTime COMMENT '".$request->parameter_name."' AFTER  `igst` "));
        }
        else
        {
            DB::statement(DB::raw("ALTER TABLE `items` ADD `".$request->parameter_id."` Text COMMENT '".$request->parameter_name."' AFTER  `igst` "));
        }
    }

    // public function getAllSubCategoryItems($sub_category_id)
    // {
    //     return DB::table('items as i')
    //     ->where('sub_categories as s','s.id','=','i.sub_category_id')
    //     ->where('sc.category_id',$category_id)
    //     ->where('i.status','=',1)
    //     ->whereDateTime('i.expires_at', '<', Carbon::now())
    //     ->lists('sc.sub_category_name', 'id');
    // }

    public function getAllSubCategoryItems($sub_category_id)
    {
         $items=DB::table('items AS i')
         ->join('sub_categories AS sc','i.sub_category_id','=','sc.id')
         ->join('unit_of_measure AS u','i.unit_of_measure','=','u.id')
         ->where('i.sub_category_id',$sub_category_id)
          ->where('status','=',1)
        ->whereDate('expires_at', '>', Carbon::now())
         ->select('i.*','sc.sub_category_name','u.display_name')
        ->get();

         return $items;
    }

     public function getCategoryDetails()
    {
       $category_details=DB::table('categories as c')
      ->join('sub_categories as sb','c.id','=','sb.category_id')
      ->select('c.category_name','sb.sub_category_name','c.id as category_id')
      ->first();

         return $category_details;
    }



}
