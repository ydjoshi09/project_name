<?php

namespace App\Models\UserDetails;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model {
	//Author : Sheevathsa
	//Date: 6/4/2018

	use SoftDeletes;
	protected $table = 'users';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'mobile_no_cc',
		'mobile_no',
		'whatsapp_no_cc',
		'whatsapp_no',
		'email',
		'password',
		'confirmation_password',
		'profile_image',
		'last_login',
		'email_verified',
		'mobile_no_verified',
		'whatsapp_no_verified',
		'profile_status',
		'user_verified_at',
		'status_remark',
		'email_verification_token',
		'user_type',
		'remember_token',
	];

	// This function will create user with the data provided by user and return the user_id in response
	protected $with = ['mobileNoCC', 'whatsAppNoCC'];

	//This function will give bank details of user
	public function mobileNoCC() {
		return $this->belongsTo('App\Models\Masters\Country', 'mobile_no_cc')->select(array('id', 'phone_code'));
	}

	public function whatsAppNoCC() {
		return $this->belongsTo('App\Models\Masters\Country', 'whatsapp_no_cc')->select(array('id', 'phone_code'));
	}

	public function store($request) {

		$user = $this->create($request);
		return $user;
	}

	public function updateEmailId($id, $email_id) {
		if ($id == Auth::user()->id) {
			$get_user = new Users();
			$user = $get_user->getTraderById($id);
			$user->email = $email_id;
			$user->email_verified = 0;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will update whatsapp_no and whatsapp_no_cc, if update successful return true else false.
	public function updateWhatsappNum($id, $whatsapp_no, $whatsapp_no_cc) {
		if ($id == Auth::user()->id) {
			$get_user = new Users();
			$user = $get_user->getTraderById($id);
			$user->whatsapp_no = $whatsapp_no;
			$user->whatsapp_no_verified = 0;
			$user->$whatsapp_no_cc = $whatsapp_no_cc;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will update mobile _no and mobile_no_cc , if update successful return true else false
	public function updateMobileNumber($id, $mobile_no, $mobile_no_cc) {
		if ($id == Auth::user()->id) {
			$user = new Users();
			$user = $user->getTraderById($id);
			$user->mobile_no = $mobile_no;
			$user->mobile_no_verified = 0;
			$user->mobile_no_cc = $mobile_no_cc;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will set email_verified bit, if update successful return true else false.
	public function verifyTraderEmail($id) {
		if ($id == Auth::user()->id) {
			$user = new Users();
			$user = $user->getTraderById($id);
			$user->email_verified = 1;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will set mobile_no_verified bit, if update successful return true else false.
	public function verifyTraderMobileNum($id) {
		if ($id == Auth::user()->id) {
			$user = new Users();
			$user = $user->getTraderById($id);
			$user->mobile_no_verified = 1;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will set whatsapp_no_verified bit, if update successful return true else false.
	public function verifyTraderWhatsappNum($id) {
		if ($id == Auth::user()->id) {
			$user = new Users();
			$user = $user->getTraderById($id);
			$user->whatsapp_no_verified = 1;
			$user->save();
			return 1;
		} else {
			return 0;
		}
	}

	// This function will update the basic details of user if user profile_status is not 5,
	public function updateTrader($id, $request) {
		$user = new Users();
		$user = $user->getTraderById($id);
		if ($user->profile_status != 5) {
			$user->update($request);
		} else {
			return 1;
		}
	}

	// This will update the profile_status of user,
	public function updateTraderStatus($id, $no) {

		// if($id==Auth::user()->id)
		// {
		$user = new Users();
		$user = $user->getTraderById($id);
		$user->profile_status = $no;
		$user->save();
		return 1;
		// }
		// else
		// {
		// 	return 0;
		// }
	}

	// This function will return Trader details which has id as provided value.
	public function getTraderById($id) {
		return $this->where('id', $id)->first();
	}
}
