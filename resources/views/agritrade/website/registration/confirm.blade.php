<div class="modal-body" id="trader_details_confirmation_div"  style="padding:1.5em 1.5em;" >
            {!!Form::open(array('method' => 'POST','id'=>'confirm-form','class'=>'confirm-form'))!!}
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6" style="
                    padding:0px;">
                    <center><h3>Basic Info</h3></center>
                    <div class="col-md-12" id="basic_info">
                    </div>
                </div>
                <div class="col-md-6" style="border-left: dotted 1px black; padding:0px;">
                    <center><h3>Statutory Details</h3></center>
                    <div class="col-md-12" id="statutory_detail">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" style="border-top: dotted 1px black;">
                    <center><h3>Bank Details</h3></center>

                    <div class="col-md-6" id="bank_data">

                    </div>
                    <div class="col-md-6" id="other_bank_data">

                    </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}
    </div>