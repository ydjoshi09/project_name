<?php

namespace App\Models\Trading;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierOrder extends Model
{
    //
    use SoftDeletes;
	protected $table='supplier_order';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'po_id',
		'supplier_id',
		'reference_document',
		'remarks',
		'supplier_po_number',
		'status',
	];

	// view_orders (1,1): this will list all the supplier orders which have status 1 confirmed .
	// view_orders (2,1): this will list all the purchase orders which have status 2 payment done by me.
	public function viewTraderOrders($status,$user_id)
	{
		return DB::table('supplier_order as so')
			->join('purchase_order as po','po.id','=','so.po_id')
			->join('cart_items as ci','ci.id','=','po.po_id')
			->join('items as i','i.id','=','ci.item_id')
			->where('so.status',$status)
			->where('i.supplier_id',$user_id)
			->lists('bank_name', 'id');
	}

	//this will return all the details of particular order
	public function view_order_details(id)
	{

	}
}
