
            <h3><b>Bank Details </b></h3>
            <hr>
            <div class="row">
                <div class="clearfix"></div>

                <input type="hidden" id="bank_user_id" name="user_id" value="0">
                <input type="hidden" name="cancelled_cheque_attachment" id="cancelled_cheque_attachment">

                <div class="col-md-4 form-group">
                    <label for="bank_name"> Bank Name*</label>
                    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Enter Bank Name">
                    <small class="error" hidden="true"></small>

                </div>
                <div class="col-md-4 form-group">
                    <label for="branch_name"> Branch Name*</label>
                    <input type="text" class="form-control" id="branch_name" name="branch_name" placeholder="Enter Branch Name">
                    <small class="error" hidden="true"></small>

                </div>



                <div class="col-md-4 form-group">

                    <label for="branch_email"> Branch Email Id*</label>
                    <input type="text" class="form-control" id="branch_email" name="branch_email" placeholder="Enter Branch Email">
                    <small class="error" hidden="true"></small>

                </div>
                <div class="clearfix"></div>

                <div class="col-md-6 form-group">

                    <label for="ifsc_code"> IFSC Code*</label>
                    <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" placeholder="Enter IFSC Code">
                    <small class="error" hidden="true"></small>

                </div>


                <div class="col-md-6 form-group">
                    <label for="micr_code"> MICR Code*</label>
                    <input type="text" class="form-control" id="micr_code" name="micr_code" placeholder="Enter MICR Code">
                    <small class="error" hidden="true"></small>

                </div>

                <div class="clearfix"></div>


                <div class="col-md-4">
                    <div class="form-group @if($errors->first('bank_country_id')) has-error @endif">
                        {!!Form::label('bank_country_id','Country *')!!}
                        {!!Form::select('bank_country_id', array(),null, ['class' => 'form-control required','id'=>'bank_country_id','name'=>'country_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>



                <div class="col-md-4" id="bank_state_div">
                    <div class="form-group @if($errors->first('bank_state_id')) has-error @endif">
                        {!!Form::label('bank_state_id','State *')!!}
                        {!!Form::select('bank_state_id', array(),null, ['class' => 'form-control required','id'=>'bank_state_id','name'=>'state_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>


                <div class="col-md-4" id="bank_city_div">
                    <div class="form-group @if($errors->first('bank_city_id')) has-error @endif">
                        {!!Form::label('bank_city_id','City *')!!}
                        {!!Form::select('bank_city_id', array(),null, ['class' => 'form-control required','id'=>'bank_city_id','name'=>'city_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>


                <div class="clearfix"></div>



                <div class="col-md-6 form-group">
                    <label for="bank_address">Address*</label>
                    <textarea class="form-control" id="bank_address" name="address" rows="3"></textarea>
                    <small class="error" hidden="true"></small>


                </div>

                <div class="clearfix"></div>


                <div class="col-md-6 form-group">
                    <label for="account_holders_name">Account Holder's Name*</label>
                    <input type="text" class="form-control" id="account_holders_name" name="account_holders_name" placeholder="Enter Account Holder's Name">
                    <small class="error" hidden="true"></small>

                </div>



                <div class="col-md-6">
                    <div class="form-group @if($errors->first('account_type')) has-error @endif">
                        {!!Form::label('account_type','Account Type*')!!}
                        {!! Form::select('account_type', array('0' => 'Select option', '1' => 'Savings ','2'=>'Current'),Input::old('account_type'),['class' => 'form-control required','id'=>'account_type'])!!}
                        <small class="text-danger">{{ $errors->first('account_type') }}</small>
                    </div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-6 form-group">
                    <label for="account_no">Account No*</label>
                    <input type="text" class="form-control" id="account_no" name="account_no" placeholder="Enter Account No">
                    <small class="error" hidden="true"></small>

                </div>




                <!-- Attach Cheque Image field -->
                <div class="col-md-6" id="other_attachment_div">
                    {!! Form::label('Cancelled Cheque Attachments*') !!}
                    <input type="file" class="form-control" id="cancelled_cheque_attachments" name="image_file">
                </div>


                <div class="clearfix" ></div>



            </div>

