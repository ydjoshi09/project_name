@extends('agritrade.layouts.main')
@section('content')

<div class="w3l_banner_nav_right">

	<div class="mail">
		<h3>Products</h3>

		<div class="w3ls_w3l_banner_nav_right_grid1">

			@foreach($sb_categories as $c)
			@if($c->item_flag==1)
			<div class="col-md-3 ">
				<div class="hover14 column">
					<a href="single.html"><div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid" style="height: 250px">
							<!-- <div class="agile_top_brand_left_grid_pos">
								<img src="/website/images/offer.png" alt=" " class="img-responsive" />
							</div> -->
							<div class="agile_top_brand_left_grid1" style="height: 230px">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">

										<?php $cate_image = ENV('BACKENDURL') . "category/" . $c->image;?>

											<a href="{{URL::to('/')}}/Website/TraderDetails/subcategory/{{$c->id}}"><img src="{{$cate_image}}" alt=" " class="img-responsive" /></a>

											<div class="clearfix"> </div><br>
											<h4>{{$c->category_name}}</h4>
										</div>

									</div>
								</figure>
							</div>
						</div></a>
					</div>
				</div>
				@endif
				@endforeach


				<div class="clearfix"> </div>
			</div>

		</div>
	</div>


	<div class="clearfix"></div>
	<!-- //banner -->
	<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<div class="w3agile_newsletter_left">
				<h3>sign up for our newsletter</h3>
			</div>
			<div class="w3agile_newsletter_right">
				<form action="#" method="post">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="submit" value="subscribe now">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //newsletter -->



	@section('script')
	@parent


	@endsection
	@stop
