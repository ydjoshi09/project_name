<?php

//Below is the list of routes for website
//index(will return home page);
//about_us
//national_distributorship
//poducts
//bill_collection
//profile_view
//seller_dashboard
//import_export

// All  functions related to this routes should be in Website controller
//Each function should return a blank web page if no data is given for that page
Route::when('*', ['middleware' => 'csrf'], ['POST', 'PUT', 'PATCH', 'DELETE']);

Route::get('/', ['as' => 'index', 'uses' => 'WebsiteController@index']);
Route::get('/about_us', ['as' => 'aboutUs', 'uses' => 'WebsiteController@aboutUs']);
Route::get('/register', ['as' => 'register', 'middleware' => 'ajax', 'uses' => 'WebsiteController@register']);
Route::get('/statutory', ['as' => 'statutory', 'middleware' => 'ajax', 'uses' => 'WebsiteController@statutory']);
Route::get('/bank_detail', ['as' => 'bank_detail', 'middleware' => 'ajax', 'uses' => 'WebsiteController@bankDetails']);
Route::get('/edit_basic_info/{id}', ['as' => 'edit_basic_info', 'middleware' => 'ajax', 'uses' => 'WebsiteController@editBasicInfo']);
Route::get('/loadlogin', ['as' => 'loadlogin', 'middleware' => 'ajax', 'uses' => 'WebsiteController@login']);
Route::get('products', ['as' => 'products', 'uses' => 'WebsiteController@products']);
Route::get('contact_us', ['as' => 'contactUs', 'uses' => 'WebsiteController@contactUs']);
Route::get('national_distributorship', ['as' => 'nationalDistributorship', 'uses' => 'WebsiteController@nationalDistributorship']);
Route::get('bill_collection', ['as' => 'billCollection', 'uses' => 'WebsiteController@billCollection']);
Route::get('import_export', ['as' => 'importExport', 'uses' => 'WebsiteController@importExport']);

//Auth
Route::group(['namespace' => 'Auth'], function () {

	Route::post('login', ['as' => 'postLogin', 'uses' => 'AuthController@doLogins']);

	Route::get('sign_out', ['as' => 'sign_out', 'uses' => 'AuthController@signOut']);

});

Route::get('get_countries', ['as' => 'getCountryCode', 'uses' => 'TraderRegistrationController@getCountryCode']);

Route::get('/get_countries_code/', ['as' => 'getCountries', 'uses' => 'TraderRegistrationController@getCountries']);

Route::get('/get_specific_states/', ['as' => 'getSpecificStates', 'uses' => 'TraderRegistrationController@getSpecificStates']);

Route::get('/get_specific_districts/', ['as' => 'getSpecificDistricts', 'uses' => 'TraderRegistrationController@getSpecificDistricts']);

//All below upload routes needs to be replaced with single route

Route::post('upload_file', ['as' => 'upload_file', 'uses' => 'FileUpload@upload']);

//In this application user will be refered as trader
//storeUserDetails will change to store stroeTraderDetails
Route::post('store_trader_details', ['as' => 'storeTraderDetails', 'uses' => 'TraderRegistrationController@storeTraderDetails']);

Route::post('storeStatutoryDetails', ['as' => 'storeStatutoryDetails', 'uses' => 'TraderRegistrationController@storeStatutoryDetails']);

Route::post('storeBankDetails', ['as' => 'storeBankDetails', 'uses' => 'TraderRegistrationController@storeBankDetails']);

Route::post('storeEditedBankDetails', ['as' => 'storeEditedBankDetails', 'uses' => 'TraderRegistrationController@storeEditedBankDetails']);

Route::post('updateShippingAddress', ['as' => 'updateShippingAddress', 'uses' => 'TraderRegistrationController@updateShippingAddress']);

Route::post('updateBillingAddress', ['as' => 'updateBillingAddress', 'uses' => 'TraderRegistrationController@updateBillingAddress']);

//Justify why below route is needed and if needed justify on name
Route::get('updateTraderStatus', ['as' => 'updateTraderStatus', 'uses' => 'TraderRegistrationController@updateTraderStatus']);

Route::post('storeBillingAddress', ['as' => 'storeBillingAddress', 'uses' => 'TraderRegistrationController@storeBillingAddress']);

Route::post('storeShippingAddress', ['as' => 'storeShippingAddress', 'uses' => 'TraderRegistrationController@storeShippingAddress']);

Route::get('/get_user_details/', ['as' => 'getUserDetails', 'uses' => 'TraderRegistrationController@getUserDetails']);

Route::post('storeEditedBasicInfo', ['as' => 'storeEditedBasicInfo', 'uses' => 'TraderRegistrationController@storeEditedBasicInfo']);

Route::get('/get_statutory_details/', ['as' => 'getStatutoryDetails', 'uses' => 'TraderRegistrationController@getStatutoryDetails']);

Route::post('storeEditedStatutoryDetails', ['as' => 'storeEditedStatutoryDetails', 'uses' => 'TraderRegistrationController@storeEditedStatutoryDetails']);

Route::get('/get_bank_details/', ['as' => 'getBankDetails', 'uses' => 'TraderRegistrationController@getBankDetails']);

Route::post('storeEditedBankInfo', ['as' => 'storeEditedBankInfo', 'uses' => 'TraderRegistrationController@storeEditedBankInfo']);

//Delete the Website Folder

Route::get('profile_view', ['as' => 'profile_view', 'uses' => 'ProfileViewController@index', 'middleware' => ['auth']]);

Route::get('seller_dashboard', ['as' => 'seller_dashboard', 'uses' => 'SellerDashboardController@index', 'middleware' => ['auth']]);

Route::get('item_details', ['as' => 'item_details', 'uses' => 'SellerDashboardController@getActiveItemDetails', 'middleware' => ['auth']]);

Route::get('expired_item_details', ['as' => 'expired_item_details', 'uses' => 'SellerDashboardController@getExpiredItemDetails', 'middleware' => ['auth']]);

Route::get('discarded_item_details', ['as' => 'discarded_item_details', 'uses' => 'SellerDashboardController@getDiscardedItemDetails', 'middleware' => ['auth']]);

Route::get('active_orders_details', ['as' => 'active_orders_details', 'uses' => 'SellerDashboardController@getActivePODetails', 'middleware' => ['auth']]);

Route::get('all_orders_details', ['as' => 'all_orders_details', 'uses' => 'SellerDashboardController@getAllPODetails', 'middleware' => ['auth']]);

Route::post('storeItemInfo', ['as' => 'Website.TraderDetails.storeItemInfo', 'uses' => 'SellerDashboardController@storeItemInfo', 'middleware' => ['auth']]);

Route::any('updateItemInfo', ['as' => 'Website.TraderDetails.updateItemInfo', 'uses' => 'SellerDashboardController@updateItemInfo', 'middleware' => ['auth']]);

Route::get('/get_sub_categories/', ['as' => 'getSubCategories', 'uses' => 'SellerDashboardController@getSubCategories', 'middleware' => ['auth']]);

Route::post('/upload_item_image/', ['as' => 'uploadItemImage', 'uses' => 'SellerDashboardController@uploadItemImage']);

Route::get('/get_parameters/', ['as' => 'getParameters', 'uses' => 'SellerDashboardController@getParameters']);

//Justify on the name and why most of the functions are in single controller
Route::get('supplier_po_view', ['as' => 'supplier_po_view', 'uses' => 'SupplierPOViewController@index']);

Route::post('updateShipmentDetails', ['as' => 'updateShipmentDetails', 'uses' => 'SupplierPOViewController@updateShipmentDetails']);

Route::get('subcategory/{id}', ['as' => 'subcategory', 'uses' => 'ProductViewController@subcategory']);

Route::get('item/{id}', ['as' => 'item', 'uses' => 'ProductViewController@item']);

Route::post('storeCartItem', ['as' => 'storeCartItem', 'uses' => 'ProductViewController@storeCartItem']);

Route::get('checkout', ['as' => 'checkout', 'uses' => 'ProductViewController@checkout']);

Route::post('removeCartItem', ['as' => 'removeCartItem', 'uses' => 'ProductViewController@removeCartItem']);

Route::post('updateCheckout', ['as' => 'updateCheckout', 'uses' => 'ProductViewController@updateCheckout']);

Route::get('addressConfirm', ['as' => 'addressConfirm', 'uses' => 'ProductViewController@addressConfirm']);

Route::post('updateActiveBillingAddress', ['as' => 'updateActiveBillingAddress', 'uses' => 'ProductViewController@updateActiveBillingAddress']);

Route::post('updateActiveShippingAddress', ['as' => 'updateActiveShippingAddress', 'uses' => 'ProductViewController@updateActiveShippingAddress']);

Route::post('updateActiveBankDetails', ['as' => 'updateActiveBankDetails', 'uses' => 'ProductViewController@updateActiveBankDetails']);

Route::get('generatePO', ['as' => 'generatePO', 'uses' => 'ProductViewController@generatePO']);

Route::get('removePO/{id}', ['as' => 'removePO', 'uses' => 'ProductViewController@removePO']);

Route::put('confirmPO', ['as' => 'Website.TraderDetails.confirmPO', 'uses' => 'ProductViewController@confirmPO']);

Route::post('updateCartShipment', ['as' => 'updateCartShipment', 'uses' => 'ProductViewController@updateCartShipment']);

Route::get('grn', ['as' => 'grn', 'uses' => 'ProductViewController@grn']);

Route::get('image/{folder_name}/{filename}', function ($folder_name, $filename) {

	$path = storage_path() . '/app/' . $folder_name . '/' . $filename;

	if (!File::exists($path)) {
		abort(404);
	}

	$file = @File::get($path);

	$type = pathinfo($filename, PATHINFO_EXTENSION);

	$response = Response::make($file, 200);
	$response->header("Content-Type", $type);

	return $response;
});

//
function getFormatedDate($date) {
	$formated_date = date("d M Y", strtotime($date));
	return $formated_date;
}
