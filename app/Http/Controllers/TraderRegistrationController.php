<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankRequest;
use App\Http\Requests\BillingAddressRequest;
use App\Http\Requests\ShippingAddressRequest;
use App\Http\Requests\StatutoryRequest;
use App\Http\Requests\UserRequest;
use App\Models\Masters\Cities;
use App\Models\Masters\Country;
use App\Models\Masters\States;
use App\Models\UserDetails\BankDetails;
use App\Models\UserDetails\BillingAddress;
use App\Models\UserDetails\ShippingAddress;
use App\Models\UserDetails\StatutoryDetails;
use App\Models\UserDetails\Users;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;

class TraderRegistrationController extends Controller {

	public function index() {

		return view('welcome');
	}

	public function home() {

		return view('website.index');

	}

	public function about() {

		return view('website.about');

	}

	public function getCountryCode(Request $request) {

		$country_codes = (new Country)->getAllCountryCodes();

		return response()->json(array('country_codes' => $country_codes));
	}

	public function getCountries(Request $request) {

		$countries = (new Country)->getAllCountries();

		return response()->json(array('countries' => $countries));
	}

	public function getSpecificStates(Request $request) {

		$states = (new States)->getSpecificStates($request->country_id);

		return response()->json(array('states' => $states));
	}

	public function getSpecificDistricts(Request $request) {

		$districts = (new Cities)->getSpecificCities($request->state_id);

		return response()->json(array('districts' => $districts));
	}

	public function storeTraderDetails(UserRequest $request) {
		$user_id = $request->user_id;
		if ($request->user_id == 0) {

			$request['password'] = bcrypt($request->password);
			$request['confirmation_password'] = bcrypt($request->confirmation_password);

			$user = (new Users())->store($request->all());

			$user_id = $user->id;
			(new Users)->updateTraderStatus($user_id, 1);
		} else {

			$user_update = (new Users)->updateTrader($request->user_id, $request->all());

		}

		return response()->json(array('user_id' => $user_id));

	}

	public function storeStatutoryDetails(StatutoryRequest $request) {

		$statutory_id = $request->statutory_id;
		if ($request->statutory_id == 0) {

			$statutory_details = (new StatutoryDetails())->store($request->all());
			$statutory_id = $statutory_details->id;
		} else {

			$statutory_details_update = (new StatutoryDetails)->updateStatutoryDetails($request->statutory_id, $request->all());

		}

		$user_details = (new Users)->updateTraderStatus($request->user_id, 2);

		return response()->json(array('statutory_id' => $statutory_id));

	}

	public function storeBankDetails(BankRequest $request) {

		(new BankDetails())->store($request->all());

		$update_user_details = (new Users)->updateTraderStatus($request->user_id, 3);

		$user_details = (new Users)->getTraderById($request->user_id);

		$statutory_details = (new StatutoryDetails)->getStatutoryDetails($request->user_id);

		$bank_details = (new BankDetails)->getBankInformation($request->user_id);

		return response()->json(array('user_details' => $user_details, 'statutory_details' => $statutory_details, 'bank_details' => $bank_details));

	}

	public function storeEditedBankDetails(BankRequest $request) {

		$request['user_id'] = $request->users_id;
		$request['cancelled_cheque_attachment'] = $request->edit_cheque_attachment_path;

		(new BankDetails())->store($request->all());

		$bank_details = (new BankDetails)->getBankInformation($request->user_id);

		return response()->json(array('bank_details' => $bank_details));

	}

	public function updateShippingAddress(ShippingAddressRequest $request) {

		$user_id = Auth::user()->id;

		$shipping_address_update = (new ShippingAddress)->updateShippingAddressDetails($request->shipping_id, $request->all());

		return 1;

	}

	public function updateBillingAddress(BillingAddressRequest $request) {

		$user_id = Auth::user()->id;

		$billing_address_update = (new BillingAddress)->updateBillingAddressDetails($request->billing_id, $request->all());

		return 1;

	}

	public function updateTraderStatus(Request $request) {

		$user_details = (new Users)->updateTraderStatus($request->u_id, 4);

		$users = DB::table('users')->where('id', $request->u_id)->select('*')->first();

		return response()->json(array('users' => $users));

	}

	public function storeBillingAddress(BillingAddressRequest $request) {

		$billing_address = (new BillingAddress())->store($request->all());

		return response()->json(array('billing_address' => $billing_address));

	}

	public function storeShippingAddress(ShippingAddressRequest $request) {

		$shipping_address = (new ShippingAddress())->store($request->all());

		return response()->json(array('shipping_address' => $shipping_address));

	}

	public function getUserDetails(Request $request) {

		$user_details = (new Users)->getTraderById($request->user_id);

		return response()->json(array('user_details' => $user_details));
	}

	public function storeEditedBasicInfo(UserRequest $request) {

		$request['profile_image'] = $request->profile_path;

		$basic_info = (new Users)->updateTrader($request->user_id, $request->except('password', 'confirmation_password'));

		return 1;

	}

	public function getStatutoryDetails(Request $request) {

		$statutory_details = (new StatutoryDetails)->getStatutoryDetailsByUserId($request->user_id);

		return response()->json(array('statutory_details' => $statutory_details));
	}

	public function storeEditedStatutoryDetails(StatutoryRequest $request) {

		$request['pan_attachment'] = $request->edit_pan_attachment_path;
		$request['gst_attachment'] = $request->edit_gst_attachment_path;
		$request['other_attachments'] = $request->edit_other_attachment_path;

		$statutory_details_update = (new StatutoryDetails)->updateStatutoryDetails($request->user_id, $request->all());

		return 1;

	}

	public function storeEditedBankInfo(BankRequest $request) {

		$user_id = Auth::user()->id;

		$request['user_id'] = $user_id;
		$request['cancelled_cheque_attachment'] = $request->edit_cheque_path;

		$bank_details_update = (new BankDetails)->updateBankDetails($request->bank_details_id, $request->all());

		return 1;

	}

}
