<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BillingAddressRequest extends Request {

	public function authorize() {
		return true;
	}

	public function rules() {

		if ($this->method() == 'PUT') {
			// Update operation, exclude the record with id from the validation:
			$billing_rule1 = 'required:billing_address,address,' . $this->get('id');

		} else {
			// Create operation. There is no id yet.
			$billing_rule1 = 'required:billing_address,address';

		}

		return [
			'address' => $billing_rule1,

			'city_id' => 'required|not_in:0',

		];
	}
}
