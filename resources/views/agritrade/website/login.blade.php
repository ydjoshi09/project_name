<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" data-backdrop="static" data-keyboard="false" aria-hidden="true" >
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="clearMe();">&times;</button>
                <center><h4 class="modal-title" >Login</h4></center>
            </div>
            <div class="modal-body" >
                {!!Form::open(array('method' => 'POST','id'=>'login-form','files'=>true))!!}
                <div id="login_error" class="alert alert-danger" hidden >Invalid Credentials</div>
                <div class="form-group" id="email_error">
                    {!! Form::label('email', 'E-Mail*');!!}
                    {!!Form::email("email", null, ['class' => 'form-control','id'=>'email','placeholder'=>'Enter Email','required']);!!}
                </div>
                <div class="form-group" id="password_error">
                    {!! Form::label('password', 'Password*');!!}
                    {!!Form::password('password',['class' => 'form-control ','id'=>'password','placeholder'=>'Enter Password','required ']);!!}
                </div>

                {!!Form::close()!!}

                <div class="checkbox">
                    <label><input type="checkbox" value="" checked>Remember me</label>
                </div>
                <!-- <button type="button" id="login_btn" name="login_btn" class="btn btn-success btn-block"> Login</button> -->
                <button type="button"  onclick="dologin();" class="btn  btn-block btn-flat bg-blue">Log In</button>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).ready(function(){
      $('#login-modal').modal();
  });

    function clearMe()
    {
        $('#login-modal').modal('hide');
        setTimeout(function() {
            $('#modal_space').html("");
        }, 2000);

    }
    function dologin()
    {
        if($('#login-form').valid()==true)
        {
            var formData = new  FormData($('#login-form')[0]);
            $.ajax({
                type: 'POST',
                url:'{{URL::route("postLogin")}}',
                data: formData,
                contentType: false,
                processData: false
            }).done(function(result)
            {

                if(result==0)
                {
                    $('#login_error').show();
                    setTimeout(function() {
                        $('#login_error').hide();
                    }, 2000);
                    $('#email_error').addClass('has-error');
                    $('#password_error').addClass('has-error');
                    $('#password_error').val("");

                }
                else
                {
                    window.location.reload();
                }
            });
        }
    }



    $('#email').on('keypress',function(){
        $('#login_error').hide();
        $('#email_error').removeClass('has-error');

    });

    $('#password').on('keypress',function(){
        $('#login_error').hide();
        $('#password_error').removeClass('has-error');

    });



</script>