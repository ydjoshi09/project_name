<div class="fixedheader">
    <div id="top">
        <div class="container nomargin full_width">
            <div class="col-md-3 logo">

                <a href="{{URL::to('/')}}">Agri Trade </a>
            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">

            </div>

            @if(isset(Auth::user()->id))
            <div class="col-md-3 login" data-animate="fadeInDown">

                <ul class="menu">
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
                      <ul class="dropdown-menu little-left">
                        <li {{ Request::is('profile_view') ? 'hidden' : '' }}><a href="{{URL::route('profile_view')}}" >My Profile</a></li>
                        @if(Auth::user()->profile_status==5)
                        <li><a href="{{URL::route('seller_dashboard')}}" >My Sales</a></li>
                        <li><a href="{{URL::route('supplier_po_view')}}" >Supplier PO View</a></li>
                        @endif
                        <li> <a href="{{URL::route('sign_out')}}" >Log out</a></li>
                    </ul>
                </li>



            </ul>
        </div>

        @else
        <div class="col-md-3 login" data-animate="fadeInDown">
            <ul class="menu">
                <li><a  onclick="loadLogin();">Login</a>
                </li>
                <li><a onclick="loadRegister();">Register</a>
                </li>
            </ul>
        </div>
        @endif

    </div>

    <div id="modal_space">
    </div>


</div>


<div class="logo_products">
    <div class="container">

        <div class="w3ls_logo_products_left1 ">
            <ul class="special_items">
                <li ><a style="{{ Request::is('/') ? 'color:#ff0000;' : '' }} " href="{{URL::route('index')}}">Home</a><i></i></li>
                <li><a style="{{ Request::is('about_us') ? 'color:#ff0000;' : '' }} "href="{{URL::route('aboutUs')}}">About Us</a><i></i></li>
                <li><a style="{{ Request::is('national_distributorship') ? 'color:#ff0000; ' : '' }} " href="{{URL::route('nationalDistributorship')}}">National Distributorship</a><i></i></li>
                <li><a style="{{ Request::is('products') ? 'color:#ff0000;' : '' }} "href="{{URL::route('products')}}">Products</a><i></i></li>
                <li><a style="{{ Request::is('bill_collection') ? 'color:#ff0000;' : '' }} " href="{{URL::route('billCollection')}}">Bill Collection</a><i></i></li>
                <li><a style="{{ Request::is('import_export') ? 'color:#ff0000;' : '' }} " href="{{URL::route('importExport')}}">Import Export</a><i></i></li>
                <li><a <a style="{{ Request::is('contact_us') ? 'color:#ff0000;' : '' }} " href="{{URL::route('contactUs')}}">Contact Us</a></li>
            </ul>

        </div>

        <div class="clearfix"> </div>
    </div>
</div>

</div>

@section('script')
<script type="text/javascript">
    var user_detail = <?php if (isset(Auth::user()->first_name)) {echo Auth::user();} else {echo 0;}?> ;

function loadRegister()
{
    $('#modal_space').load('{{URL::route("register")}}');

}

function loadLogin()
{
    $('#modal_space').load('{{URL::route("loadlogin")}}');
}






</script>
