<?php

namespace App\Http\Controllers\Website\Template;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;
use Session;
use Auth;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('website.template.index');
    }

    public function about()
    {
        return view('website.template.about');
    }

    public function contact()
    {
        return view('website.template.contact');
    }

    public function products()
    { 
        return view('website.template.products');
    }

    public function login()
    {
        return view('website.template.login');
    }

}