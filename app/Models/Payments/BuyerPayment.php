<?php

namespace App\Models\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyerPayment extends Model
{
    //Author : Sheevathsa
    //Date: 7/4/2018
    use SoftDeletes;
	protected $table='buyer_payments';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'po_id',
        'payment_amount',
        'tax_amount',
        'payment_status',
        'received_amount',
        'transaction_id',
	];

    // This function will generate payment entry for the po.
    public function store($request)
    {
        return $this->create($request->all());
    }
}
