@extends('agritrade.layouts.main')

@section('title','Dashboard')

@section('page_title_sub', 'Place where it start\'s')

@section('content')
   <div class='row'>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="{{URL::to('/')}}/website/images/1.jpg" alt="Chania">

    </div>

    <div class="item">
      <img src="{{URL::to('/')}}/website/images/2.jpg" alt="Chicago">

    </div>

    <div class="item">
      <img src="{{URL::to('/')}}/website/images/3.jpg" alt="New York">

    </div>
    <div class="item">
      <img src="{{URL::to('/')}}/website/images/4.jpg" alt="New York">

    </div>
  </div>


</div>
   </div>
@endsection

@section('script')
@parent

<script type="text/javascript">



</script>
@stop
