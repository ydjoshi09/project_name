<div id="my_form">
{!!Form::open(array('method' => 'POST','id'=>'statutory-form','class'=>'statutory-form'))!!}

@include('agritrade.website.registration.statutorydetails._form')

{!!Form::close()!!}
</div>



<script type="text/javascript">


	$(document).ready(function()
	{
		$('#back_to_user_details').removeAttr("disabled");
		$("#back_to_user_details").show();
		$("#statutory_next_btn").show();
		$('#next_btn').hide();
		$('#back_to_statutory_details').hide();
		$('#bank_details_next_btn').hide();
		$('#trader_details_confirmation_div').hide();
		$('#confirm_btn').hide();
	});


	$('#back_to_user_details').click(function(){
		$('#back_to_user_details').attr("disabled", "disabled");
		$('#modal_body').empty();
		$('#modal_body').load('{{URL::to("/")}}/edit_basic_info/'+$('#user_id').val());
		$("#back_to_user_details").hide();
		$("#statutory_next_btn").hide();
		$('#next_btn').show();
		$('#back_to_statutory_details').hide();
		$('#bank_details_next_btn').hide();
		$('#trader_details_confirmation_div').hide();
		$('#confirm_btn').hide();
	});

	$('#country_id').change(function(){
		$('#state_ids').html('');
		$('#city_id').html('');
		var country_id=$('#country_id').val();

		$.ajax({
			type: 'GET',
			url:'{{URL::route("getSpecificStates")}}',
			dataType: 'json',
			data:{country_id:country_id},
			async:false
		}).done(function(result){
			result=result['states'];
			var dis='<option value=0>Select State</option>';
			for(i=0;i<result.length;i++){
				dis+='<option value='+result[i]['id']+'>'+result[i]['state_name']+'</option>';
			}
			$('#state_ids').html(dis).selectpicker();

		});



		$('#state_ids').change(function(){
			$('#city_id').html('');
			var state_id=$('#state_ids').val();
			$.ajax({
				type: 'GET',
				url:'{{URL::route("getSpecificDistricts")}}',
				dataType: 'json',
				data:{state_id:state_id},
				async:false
			}).done(function(result){
				result=result['districts'];
				var dis='<option value=0>Select City</option>';
				for(i=0;i<result.length;i++){
					dis+='<option value='+result[i]['id']+'>'+result[i]['city_name']+'</option>';
				}
				$('#city_id').html(dis).selectpicker();
			});
		})
	})



	$.ajax({

		type: 'GET',
		url:'get_countries',
		dataType: 'json',
		async:false
	}).done(function(result){
		result=result['country_codes'];
		var dis1='<option value=0>Select</option>';
		var countries="'<option value=0>Select</option>'"
		for(i=0;i<result.length;i++){
			if(result[i]['id']==101)
			{

				countries+='<option selected value='+result[i]['id']+'>'+result[i]['country_name']+'</option>';
			}
			else
			{

				countries+='<option value='+result[i]['id']+'>'+result[i]['country_name']+'</option>';
			}
		}



		$('#country_id').html(countries).selectpicker();
		$('#country_id').trigger('change');


	});

	$('#statutory_next_btn').click(function(){
		$('#modal_body').load('{{URL::route("bank_detail")}}');
		$('#back_to_user_details').removeAttr("disabled");
		$("#back_to_user_details").show();
		$("#statutory_next_btn").show();
		$('#next_btn').hide();
		$('#back_to_statutory_details').hide();
		$('#bank_details_next_btn').hide();
		$('#trader_details_confirmation_div').hide();
		$('#confirm_btn').hide();
	});


	function next()
	{
		var formData = new FormData($('#statutory-form')[0]);

		if(1){

			var ajax = $.ajax({
				type: 'POST',
				url:'{{URL::route("storeStatutoryDetails")}}',

				data: formData,
				contentType: false,
				processData: false
			}).done(function(result) {
				if(result){

					$('#statutory_id').val(result['statutory_id']);
					$('#basic_info_div').hide();
					$('#statutory_details_div').hide();
					$('#bank_details_div').show();

					$('#next_btn').hide();
					$("#statutory_next_btn").hide();
					$('#bank_details_next_btn').show();

					$("#back_to_user_details").hide();
					$('#back_to_statutory_details').show();
					$('#trader_details_confirmation_div').hide();
					$('#confirm_btn').hide();


				}
			}).fail(function (result) {

				var obj=jQuery.parseJSON(result['responseText']);
				var keys =Object.keys(obj);


				for(i=0;i<keys.length;i++)
				{

					$('span').next('.error').remove();
					$('input[type="file"]').next('.error').remove();
					$('textarea').next('.error').remove();


					$('input[name="' + keys[i] + '"], select[name="' + keys[i] + '"]').addClass('error').after('<span class="error">'+obj[keys[i]]+'</span>');

					$('textarea[name="' + keys[i] + '"]').addClass('error').after('<span class="error">'+obj[keys[i]]+'</span>');

				}

			});
		}

	}

</script>

