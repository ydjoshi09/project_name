<?php

namespace App\Models\Trading;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItems extends Model {
	//
	//

	//Reviewed By : Yogesh J
	//Date : 09/05/2018
	use SoftDeletes;
	protected $table = 'cart_items';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'item_id',
		'buyer_id',
		'po_id',
		'quantity',
		'shipment_flag',

	];

	// This function will create new entry in cart items table.
	public function store($user_id, $request) {
		$request['buyer_id'] = $user_id;
		$request['po_id'] = 0;
		return $this->create($request->all());
	}

	//This function will set po_id of cart item table.
	public function updatePOId($id, $po_id, $quantity) {
		$item = $this->where('id', '=', $id)->first();
		$item->po_id = $po_id;
		$item->quantity = $quantity;

		return $item->save();
	}

	public function updatePOIdEmpty($po_id) {
		$item = $this->where('po_id', '=', $po_id)->get();

		foreach ($item as $c) {
			$c->po_id = 0;

			$c->save();
		}

		return 1;
	}

	// This function will return entry from cart items table.
	public function getPOItems($user_id, $po_id) {

		return DB::table('cart_items as ci')->join('items as i', 'i.id', '=', 'ci.item_id')->where('supplier_id', '=', $user_id)->where('po_id', '=', $po_id)->select('ci.*', 'i.item_code', 'i.item_name', 'i.item_image', 'i.package_quantity', 'i.package_cost', 'i.min_order_quantity', 'i.max_order_quantity', 'i.igst', 'i.cgst', 'i.sgst')->get();
	}

	// update_shipment_flag(1,1): this will set the item status to 1 that is shipped by supplier
	// update_shipment_flag(1,2): this will set the item status to 2 that is received by buyer
	public function updateShipmentFlag($id, $no) {
		$item = $this->where('id', $id)->first();
		$item->shipment_flag = $no;
		return $item->save();
	}

	//This function will set shipping date to current time.
	public function updateShippingDate($id, $shipment_date) {
		$item = $this->where('id', $id)->first();
		$item->received_date = Carbon::now();
		$item->shipment_date = date('Y-m-d', strtotime($shipment_date));
		return $item->save();
	}
	//This function will set received date to current time.
	public function updateReceivedDate($id) {
		$item = $this->where('id', $id)->first();
		$item->received_date = Carbon::now();
		return $item->save();
	}

	// this function will return all items for use which as po_id as 0.
	public function getCartItems($user_id) {
		return DB::table('cart_items as ci')->join('items as i', 'i.id', '=', 'ci.item_id')->where('i.supplier_id', '=', $user_id)->where('ci.po_id', 0)->select('ci.*', 'i.item_code', 'i.item_name', 'i.item_image', 'i.package_quantity', 'i.package_cost', 'i.min_order_quantity', 'i.max_order_quantity', 'i.igst', 'i.cgst', 'i.sgst')->get();
	}

	public function removeCartItem($id) {
		return DB::table('cart_items')->where('id', $id)->delete();
	}
}
