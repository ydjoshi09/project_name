
$('#password_field').hide();
$('#confirmation_password_field').hide();

  $('#img').attr('src',"/image/profile_images/"+user['profile_image']);

$('#user_id').val(user['id']);

    $.ajax({

    type: 'GET',
    url:'get_countries',
    dataType: 'json',
    async:false
}).done(function(result){
    result=result['country_codes'];
    var country_codes='<option value=0>Select</option>';
    for(i=0;i<result.length;i++){
        if(result[i]['id']==user['mobile_no_cc'])
        {
            country_codes+='<option selected value='+result[i]['id']+'>'+"+"+result[i]['phone_code']+'</option>';

        }
        else
        {
            country_codes+='<option value='+result[i]['id']+'>'+"+"+result[i]['phone_code']+'</option>';

        }
    }

    $('#mobile_no_cc').html(country_codes).selectpicker();
    country_codes='<option value=0>Select</option>';
    for(i=0;i<result.length;i++){
        if(result[i]['id']==user['whatsapp_no_cc'])
        {
            country_codes+='<option selected value='+result[i]['id']+'>'+"+"+result[i]['phone_code']+'</option>';

        }
        else
        {
            country_codes+='<option value='+result[i]['id']+'>'+"+"+result[i]['phone_code']+'</option>';

        }
    }
    $('#whatsapp_no_cc').html(country_codes).selectpicker();



});



$('#image_file').change(function(){


    var formData = new FormData($('#basic-form')[0]);
    formData.append('store_folder', 'profile_images');
    var ajax = $.ajax({
        type: 'POST',
        url:'upload_file',
        data: formData,
        contentType: false,
        processData: false
    }).done(function(result) {
        $('#profile_image').val(result);
        $('#img').attr('src',"/image/profile_images/"+result);

    })
    .fail(function() {
        alert("fail");
    });

});




$("#basic-form input[id=first_name]").keypress(function(e) {
    if (e.which !== 0) {
        $('#first_name_field').removeClass('has-error');
        $('#first_name_error').html("").show();
    }
});

$("#basic-form input[id=last_name]").keypress(function(e) {
    if (e.which !== 0) {
        $('#last_name_field').removeClass('has-error');
        $('#last_name_error').html("").show();
    }
});

$("#basic-form input[id=mobile_no]").keypress(function(e) {
    if (e.which !== 0) {
        $('#mobile_no_field').removeClass('has-error');
        $('#mobile_no_error').html("").show();
    }
});

$("#basic-form input[id=whatsapp_no]").keypress(function(e) {
    if (e.which !== 0) {
        $('#whatsapp_no_field').removeClass('has-error');
        $('#whatsapp_no_error').html("").show();
    }
});

$("#basic-form input[id=email]").keypress(function(e) {
    if (e.which !== 0) {
        $('#email_field').removeClass('has-error');
        $('#email_error').html("").show();
    }
});

$("#basic-form input[id=password]").keypress(function(e) {
    if (e.which !== 0) {
        $('#password_field').removeClass('has-error');
        $('#password_error').html("").show();
    }
});

$("#basic-form input[id=confirmation_password]").keypress(function(e) {
    if (e.which !== 0) {
        $('#confirmation_password_field').removeClass('has-error');
        $('#confirmation_password_error').html("").show();
    }
});


$('#next_btn').click(function(){
    var flag=0;
    var formData = new FormData($('#basic-form')[0]);
    formData.append('user_id',$('#user_id').val());
    if(formData.get('first_name')=="")
    {
        $('#first_name_field').addClass('has-error');
        $('#first_name_error').html("The First Name field is required.").show();
        flag=1;
    }

    if(formData.get('last_name')=="")
    {
        $('#last_name_field').addClass('has-error');
        $('#last_name_error').html("The Last Name field is required.").show();
        flag=1;
    }

    if(formData.get('mobile_no')=="")
    {
        $('#mobile_no_field').addClass('has-error');
        $('#mobile_no_error').html("The Mobile No field is required.").show();
        flag=1;
    }

    if(formData.get('whatsapp_no')=="")
    {
        $('#whatsapp_no_field').addClass('has-error');
        $('#whatsapp_no_error').html("The WhatsApp No field is required.").show();
        flag=1;
    }

    if(formData.get('email')=="")
    {
        $('#email_field').addClass('has-error');
        $('#email_error').html("The Email Id field is required.").show();
        flag=1;
    }


    if(formData.get('profile_image')=="")
    {
        $('#profile_attachment_field').addClass('has-error');
        $('#profile_attachment_error').html("The Profile Image is required.").show();
        flag=1;
    }





    if(flag==0){
        var ajax = $.ajax({
            type: 'POST',
            url:'store_trader_details',

            data: formData,
            contentType: false,
            processData: false
        }).done(function(result) {

            if(result['user_id']>0){
                $('#modal_body').empty();
               $('#modal_body').load('{{URL::route("statutory")}}');
            }

        }).fail(function (result) {

            var obj=jQuery.parseJSON(result['responseText']);
            var keys =Object.keys(obj);

            for(i=0;i<keys.length;i++)
            {
                $('#'+keys[i]+'_field').addClass('has-error');
                var error=obj[keys[i]][0];
                var key_value=keys[i];
                key_value=key_value.replace('_',' ');
                var new_key_value   =toTitleCase(key_value);
                new_key_value   =new_key_value.replace('Confirmation','Confirm');
                $('#'+keys[i]+'_error').html(error.replace(key_value,new_key_value));
                $('#'+keys[i]+'_error').show();
                console.log(obj[keys[i]]);
                if(keys[i]=='image_file')
                {
                    $('#profile_attachment_field').addClass('has-error');
                    $('#profile_attachment_error').html('The Profile Image field is required.');
                    $('#profile_attachment_error').show();
                }
            }

        });

    }


});
