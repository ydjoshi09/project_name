<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class TemplateController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('website.template.index');
	}

	public function about() {
		return view('website.template.about');
	}

	public function contact() {
		return view('website.template.contact');
	}

	public function products() {
		return view('website.template.products');
	}

	public function login() {
		return view('website.template.login');
	}

}