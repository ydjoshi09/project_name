<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Trading\CartItems;
use App\Models\Trading\PurchaseOrder;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Response;

class SupplierPOViewController extends Controller {

	public function index() {
		$user_id = Auth::user()->id;

		$active_orders = (new PurchaseOrder)->getPurchaseOrders([1], $user_id);

		$active_orders = PurchaseOrder::where(DB::raw('Date(po_date)'), '>=', Carbon::now())->where('buyer_id', Auth::user()->id)->where('status', 1)->get();

		foreach ($active_orders as $ac) {

			$item_details = (new PurchaseOrder)->getShipmentPODetails($ac->id, 0);

			$ac->item_details = $item_details;

		}

		$active_items = [];
		$expired_items = [];
		$all_orders = [];
		$discarded_items = [];

		return view('website.trader_details.supplier_po_view')->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('user_id', $user_id);
	}

	public function updateShipmentDetails(Request $request) {

		$shipment_date = Carbon::now();

		$active_orders = (new CartItems)->updateShipmentFlag($request->cart_id, 1);

		$active_orders = (new CartItems)->updateShippingDate($request->cart_id, $shipment_date);

		return response(1);

	}

}
