<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitOfMeasure extends Model
{
     use SoftDeletes;
	protected $table='unit_of_measure';
	protected $dates = ['deleted_at'];
	protected $fillable = [
			'unit_name',
            'display_name',
            'created_by',
            'updated_by'
	];

	// returns unit of measure
    public function getUnitofMeasures()
    {
        return $this->get();
    }
}
