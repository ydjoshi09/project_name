<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemRequest extends Request
{
    
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {

        if ($this->method() == 'put')
        {
            // Update operation, exclude the record with id from the validation:
            $item_rule1= 'required:items,item_name,' . $this->get('id');
            $item_rule4 = 'required:items,package_quantity,' . $this->get('id');
            $item_rule5 = 'required:items,package_cost,' . $this->get('id');
            $item_rule6 = 'required:items,min_order_quantity    ,' . $this->get('id');
            $item_rule8 = 'required:items,max_order_quantity    ,' . $this->get('id');
            $item_rule9 = 'required:items,igst    ,' . $this->get('id');
            $item_rule10 = 'required:items,cgst    ,' . $this->get('id');
            $item_rule11= 'required:items,sgst    ,' . $this->get('id');
            $item_rule12= 'required:items,item_image    ,' . $this->get('id');






        }
        else
        {
            // Create operation. There is no id yet.
             $item_rule1= 'required:items,item_name' ;
            $item_rule4 = 'required:items,package_quantity' ;
            $item_rule5 = 'required:items,package_cost' ;
            $item_rule6 = 'required:items,min_order_quantity' ;
            $item_rule8 = 'required:items,max_order_quantity ' ;
            $item_rule9 = 'required:items,igst' ;
            $item_rule10 = 'required:items,cgst' ;
            $item_rule11= 'required:items,sgst' ;
            $item_rule12= 'required:items,item_image' ;

        }
        
    
        return [
            'category_id'=>'required',
            // 'sub_category_id'=>'required',
            'uom'=>'required|not_in:0',

            'item_name'=>$item_rule1,
          
            'package_quantity'=>$item_rule4,
            'package_cost'=>$item_rule5,
            'min_order_quantity'=>$item_rule6,
             'max_order_quantity'=>$item_rule8,
            'igst'=>$item_rule9,
            'cgst'=>$item_rule10,
            'sgst'=>$item_rule11,
            'item_image_file'=>$item_rule12,


           
        ];
    }
}
