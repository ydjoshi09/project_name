<?php

namespace App\Http\Controllers;
use App;
use App\Http\Controllers\Controller;
use App\Models\Masters\SubCategory;
use App\Models\Payments\BuyerPayment;
use App\Models\Trading\CartItems;
use App\Models\Trading\Items;
use App\Models\Trading\PurchaseOrder;
use App\Models\UserDetails\BankDetails;
use App\Models\UserDetails\BillingAddress;
use App\Models\UserDetails\ShippingAddress;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ProductViewController extends Controller {

	public function subcategory($id) {
		$subcategories = (new SubCategory)->getAllSubCategories($id);

		foreach ($subcategories as $c) {
			$c->item_flag = 0;
		}

		foreach ($subcategories as $sc) {
			foreach ($subcategories as $sc) {
				$items = (new Items)->getAllSubCategoryItems($sc->id);

				if (count($items) > 0) {
					$sc->item_flag = 1;
				}
			}
		}

		return view('website.trader_details.subcategory')->with('subcategories', $subcategories);
	}

	public function item($id) {
		$items = (new Items)->getAllSubCategoryItems($id);

		return view('website.trader_details.item')->with('items', $items);
	}

	public function storeCartItem(Request $request) {

		$user_id = Auth::user()->id;

		if ($user_id > 0) {

			$cart_items = (new CartItems)->store($user_id, $request);

			return response(1);
		} else {

			return response(0);
		}
	}

	public function checkout() {
		$user_id = Auth::user()->id;

		$getCartItems = (new CartItems)->getCartItems($user_id);

		return view('website.trader_details.checkout')->with('getCartItems', $getCartItems);
	}

	public function removeCartItem(Request $request) {
		(new CartItems)->removeCartItem($request->id);

		return response(1);
	}

	public function updateCheckout(Request $request) {

		$user_id = Auth::user()->id;
		$request['buyer_id'] = $user_id;
		$request['po_date'] = Carbon::now();

		$billing_address = (new BillingAddress)->getActiveBillingAddressDetail($user_id);
		$request['billing_address'] = $billing_address->id;

		$shipping_address = (new ShippingAddress)->getActiveShippingAddressDetail($user_id);
		$request['shipping_address'] = $shipping_address->id;

		$bank_details = (new BankDetails)->getActiveBankDetails($user_id);

		$request['bank_details'] = $bank_details->id;

		$po = (new PurchaseOrder)->store($request);

		foreach ($request->item_array as $ia) {
			(new CartItems)->updatePOId($ia['cart_id'], $po->id, $ia['quantity']);
		}

		return response(1);
	}

	public function addressConfirm() {
		$user_id = Auth::user()->id;

		$user_billing_address = (new BillingAddress)->getUserBillingAddressDetail($user_id);

		$user_shipping_address = (new ShippingAddress)->getUserShippingAddressDetail($user_id);

		$user_bank_details = (new BankDetails)->getBankDetails($user_id);

		return view('website.trader_details.address_confirm')->with('user_bank_details', $user_bank_details)->with('user_shipping_address', $user_shipping_address)->with('user_billing_address', $user_billing_address);
	}

	public function updateActiveBillingAddress(Request $request) {
		$user_id = Auth::user()->id;
		(new BillingAddress)->updateActiveBillingAddressDetails($request->id, $user_id);

		return response(1);
	}

	public function updateActiveShippingAddress(Request $request) {
		$user_id = Auth::user()->id;
		(new ShippingAddress)->updateActiveShippingAddressDetails($request->id, $user_id);

		return response(1);
	}

	public function updateActiveBankDetails(Request $request) {
		$user_id = Auth::user()->id;
		(new BankDetails)->updateActiveBankDetails($request->id, $user_id);

		return response(1);
	}

	public function generatePO(Request $request) {
		$user_id = Auth::user()->id;

		$purchase_order = (new PurchaseOrder)->getPODeatils(0, $user_id);

		if ($purchase_order) {
			$po_items = (new CartItems)->getPOItems($user_id, $purchase_order->id);

			return view('website.trader_details.generate_po')->with('purchase_order', $purchase_order)->with('po_items', $po_items);
		}
	}

	public function confirmPO(Request $request) {
		$user_id = Auth::user()->id;

		$po_items = (new CartItems)->getPOItems($user_id, $request->po_id);

		$po = getPOData($request->po_id);

		$html = view('website.trader_details.po_pdf')->with('purchase_order', $po)->with('po_items', $po_items)->render();

		ini_set('max_execution_time', 600);

		$pdf_filename = 'pdf_po';

		$file_path = storage_path('app' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $pdf_filename . '.pdf');
		$snappy = App::make('snappy.pdf');

		$snappy->generateFromHtml($html, $file_path, [], $overwrite = true, array('encoding' => 'utf-8'));

		$filename = $pdf_filename;
		header('Content-type: application/pdf');
		header('Content-charset: x-iscii-ka');
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header('Content-Transfer-Encoding: binary');
		header('Accept-Ranges: bytes');
		@readfile($file_path);

		(new PurchaseOrder)->updatePdfAttachment($request->po_id, $file_path);

		$request['po_id'] = $request->po_id;
		$request['payment_amount'] = $request->payment_amount;
		$request['tax_amount'] = $request->tax_amount;
		(new BuyerPayment)->store($request);

		return view('website.trader_details.grn')->with('purchase_order', $final_array['purchase_orders'])->with('po_items', $po_items);
	}

	public function removePO($id) {
		(new PurchaseOrder)->deletePO($id);

		(new CartItems)->updatePOIdEmpty($id);

		$user_id = 1;

		$getCartItems = (new CartItems)->getCartItems($user_id);

		return view('website.trader_details.checkout')->with('getCartItems', $getCartItems);
	}

	public function grn(Request $request) {

		$user_id = Auth::user()->id;

		$purchase_orders = (new PurchaseOrder)->getPODeatils(1, $user_id);

		if ($purchase_orders) {

			foreach ($purchase_orders as $po) {

				$cart_items = (new CartItems)->getPOItems($user_id, $po->id);

				$po->po_items = $cart_items;
			}
		} else {

			return redirect()->route('index');
		}

		return view('website.trader_details.grn')->with('purchase_orders', $purchase_orders);
	}

	public function updateCartShipment(Request $request) {
		(new CartItems)->updateShipmentFlag($request->id, 1);

		(new CartItems)->updateShippingDate($request->id, $request->shipment_date);

		$count = CartItems::where('po_id', $request->po_id)->where('shipment_flag', 0)->count();

		if ($count == 0) {
			DB::table('purchase_order')->where('id', $request->po_id)->update(['status' => 4]);
		}

		return response(1);
	}

}

function getPOData($id) {

	$purchase_order = (new PurchaseOrder)->getAllPODetailsById($id);

	return $purchase_order;
}
