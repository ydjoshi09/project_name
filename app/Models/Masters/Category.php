<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = [
  'category_name',
  'description',
  'image',
  'created_by',
  'updated_by'
  ];

  public function getAllCategories()
  {
    return $this->whereNull('deleted_at')->get(); 
  }
}


