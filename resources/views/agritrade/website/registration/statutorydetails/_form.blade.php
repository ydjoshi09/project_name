
            <h3><b>Statutory Details </b></h3>
            <hr>
            <div class="row">
                <div class="clearfix"></div>
                <input type="hidden" id="statutory_id"   name="statutory_id" value="0">
                <input type="hidden" id="statutory_user_id" name="user_id" value="0">
                <input type="hidden" id="pan_attachment" name="pan_attachment" value="">
                <input type="hidden" id="gst_attachment" name="gst_attachment" value="">
                <input type="hidden" id="other_attachments" name="other_attachments" value="">
                <div class="col-md-4 form-group">
                    <label for="firm_name"> Firm Name*</label>
                    <input type="text" class="form-control" id="firm_name" name="firm_name" placeholder="Enter Firm Name">
                </div>


                <div class="col-md-4">
                    <div class="form-group @if($errors->first('type_of_firm')) has-error @endif">
                        {!!Form::label('type_of_firm','Firm Type*')!!}
                        {!! Form::select('type_of_firm', array('0' => 'Select option', '1' => 'Sole Proprietor','2'=>'Partnership','3'=>'Co-Op Society','4'=>'PLC','5'=>'PPL','6'=>'Farmer Cluster'),Input::old('type_of_firm'),['class' => 'form-control required','id'=>'type_of_firm'])!!}
                        <small class="text-danger">{{ $errors->first('type_of_firm') }}</small>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6 form-group">
                    <label for="pan_no"> PAN Number*</label>
                    <input type="text" class="form-control" id="pan_no" name="pan_no" placeholder="Enter PAN Number">
                    <small class="error" hidden="true"></small>

                </div>



                <div class="col-md-6 form-group">
                    <label for="gst_no"> GST Number*</label>
                    <input type="text" class="form-control" id="gst_no" name="gst_no" placeholder="Enter GST Number">
                    <small class="error" hidden="true"></small>

                </div>


                <div class="clearfix"></div>


                <div class="col-md-4">
                    <div class="form-group @if($errors->first('country_id')) has-error @endif">
                        {!!Form::label('country_id','Country *')!!}
                        {!!Form::select('country_id', array(),null, ['class' => 'form-control required','id'=>'country_id','name'=>'country_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>


                <div class="col-md-4" id="state_div">
                    <div class="form-group @if($errors->first('state_ids')) has-error @endif">
                        {!!Form::label('state_ids','State *')!!}
                        {!!Form::select('state_ids', array(),null, ['class' => 'form-control required','id'=>'state_ids','name'=>'state_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>


                <div class="col-md-4" id="city_div">
                    <div class="form-group @if($errors->first('city_id')) has-error @endif">
                        {!!Form::label('city_id','City *')!!}
                        {!!Form::select('city_id', array(),null, ['class' => 'form-control','id'=>'city_id','name'=>'city_id','data-live-search'=>'true']) !!}
                        <small class="error" hidden="true"></small>
                    </div>
                </div>

                <div class="clearfix"></div>


                <div class="col-md-6 form-group">
                    <label for="address">Address*</label>
                    <textarea class="form-control" id="address" name="address" rows="3"></textarea>
                    <small class="error" hidden="true"></small>


                </div>


                <div class="clearfix"></div>



                <div>
                    <!-- Attach PAN Image field -->
                    <div class="col-md-4" id="pan_attachment_div">
                        {!! Form::label('PAN Attachment*') !!}
                        <input type="file" class="form-control" id="pan_attachment_upload" name="pan_image_file">
                    </div>


                </div>




                <div>
                    <!-- Attach GST Image field -->
                    <div class="col-md-4" id="gst_attachment_div">
                        {!! Form::label('GST Attachment*') !!}
                        <input type="file" class="form-control" id="gst_attachment_upload" name="gst_image_file">
                    </div>


                </div>


                <div>
                    <!-- Attach Other Image field -->
                    <div class="col-md-4" id="other_attachment_div">
                        {!! Form::label('Other Attachments') !!}
                        <input type="file" class="form-control" id="other_attachments_upload" name="image_file">
                    </div>


                    <div class="clearfix" ></div>
                </div>

            </div>
