<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Country extends Model
{
    //
    use SoftDeletes;
	protected $table='countries';

	protected $fillable = [
		'country_name',
		'phone_code',
	];

	// This function will return all countries.
	 public function getAllCountries()
    {
       
      return DB::table('countries')->get();

    }

    // This function will return all country codes.
	 public function getAllCountryCodes()
    {  

    		
      return DB::table('countries')->get();

  

    }

	
	
}
