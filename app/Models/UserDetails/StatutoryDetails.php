<?php

namespace App\Models\UserDetails;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatutoryDetails extends Model {
	//Author : Sheevathsa
	//Date: 6/4/2018
	use SoftDeletes;
	protected $table = 'statutory_details';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'user_id',
		'firm_name',
		'type_of_firm',
		'pan_no',
		'pan_attachment',
		'pan_verified',
		'gst_no',
		'gst_attachment',
		'gst_verified',
		'city_id',
		'address',
		'other_attachments',
	];

	protected $with = array('city');

	public function city() {
		return $this->belongsTo('App\Models\Masters\City', 'city_id');
	}
	//store($request) -This function will create new statutory_details entry and return details with id as response.

	public function store($request) {

		$statutory_details = $this->create($request);
		return $statutory_details;
	}
	//updateStatutoryDetails($id,$request) - The function will save new values from $request to the row which has id as $id

	public function updateStatutoryDetails($user_id, $request) {

		$statutory_details = new StatutoryDetails();
		$statutory_details = $statutory_details->getStatutoryDetailsById($user_id);
		$statutory_details->update($request);
		return 1;
	}

	//getStatutoryDetailsById($id) - this function will return table row from statutory details table where id is $id
	public function getStatutoryDetailsById($id) {
		return $this->where('user_id', $id)->first();
	}

	//function getStatutoryDetailsByUserId($user_id) - this function will return table row from statutory details table where user_id is $user_id
	public function getStatutoryDetailsByUserId($user_id) {
		return $this->where('user_id', $user_id)->first();
	}

	//getStatutoryDetails($user_id) - this function will return all details from statutory details table,countries,states,cities table where user_id is $user_id

	public function getStatutoryDetails($user_id) {

		$statutory_details = DB::table('statutory_details as st')
			->join('cities AS c', 'c.id', '=', 'st.city_id')
			->join('states AS s', 's.id', '=', 'c.state_id')
			->join('countries AS co', 'co.id', '=', 's.country_id')
			->select('st.*', 'co.id as country_id', 'co.country_name', 'co.phone_code', 's.id as state_id', 's.state_name', 'c.id as city_id', 'c.city_name')
			->where('st.user_id', $user_id)
			->first();

		return $statutory_details;

	}

}