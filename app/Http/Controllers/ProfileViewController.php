<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Masters\Country;
use App\Models\UserDetails\BankDetails;
use App\Models\UserDetails\BillingAddress;
use App\Models\UserDetails\ShippingAddress;
use App\Models\UserDetails\StatutoryDetails;
use App\Models\UserDetails\Users;
use Auth;

class ProfileViewController extends Controller {

	public function index() {
		$user_id = Auth::user()->id;

		$user_details = (new Users)->getTraderById($user_id);

		$statutory_details = (new StatutoryDetails)->getStatutoryDetails($user_id);

		$bank_details = (new BankDetails)->getUserBankDetails($user_id);

		$billing_details = (new BillingAddress)->getUserBillingAddressDetail($user_id);

		$shipping_details = (new ShippingAddress)->getUserShippingAddressDetail($user_id);

		$countries = (new Country)->getAllCountries();
		return view('website.trader_details.profile_view')->with('user_id', $user_id)->with('user_details', $user_details)->with('statutory_details', $statutory_details)->with('bank_details', $bank_details)->with('billing_details', $billing_details)->with('shipping_details', $shipping_details)->with('countries', $countries);
	}

}
