<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemRequest;
use App\Models\Masters\Category;
use App\Models\Masters\SubCategory;
use App\Models\Masters\SubCategoryToParameterMapping;
use App\Models\Masters\UnitOfMeasure;
use App\Models\Trading\Items;
use App\Models\Trading\PurchaseOrder;
use Auth;
use Illuminate\Http\Request;
use Response;

class SellerDashboardController extends Controller {

	public function index() {
		$user_id = Auth::user()->id;

		$active_orders = (new PurchaseOrder)->getPurchaseOrders([1, 2, 3], $user_id)->count();

		$all_orders = (new PurchaseOrder)->getPurchaseOrders([4], $user_id)->count();

		$active_items = (new Items)->getActiveItems($user_id)->count();

		$expired_items = (new Items)->getExpiredItems($user_id)->count();

		$discarded_items = (new Items)->getDiscardedItems($user_id)->count();

		return view('website.trader_details.seller_dashboard')->with('active_orders', $active_orders)->with('all_orders', $all_orders)->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items);
	}

	public function getActiveItemDetails() {

		$user_id = Auth::user()->id;

		$active_items = (new Items)->getActiveItems($user_id);

		foreach ($active_items as $ac) {

			$category_details = (new Items)->getCategoryDetails();

			$ac->category_details = $category_details;

		}

		$expired_items = [];
		$discarded_items = [];
		$all_orders = [];
		$active_orders = [];

		$categories = (new Category)->getAllCategories();

		$uom = (new UnitOfMeasure)->getUnitofMeasures();

		return view('website.trader_details.item_details')->with('user_id', $user_id)->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('categories', $categories)->with('uom', $uom);
	}

	public function getExpiredItemDetails() {

		$user_id = Auth::user()->id;

		$expired_items = (new Items)->getExpiredItems($user_id);

		foreach ($expired_items as $ac) {

			$category_details = (new Items)->getCategoryDetails();

			$ac->category_details = $category_details;

		}

		$active_items = [];
		$discarded_items = [];
		$all_orders = [];
		$active_orders = [];

		$categories = (new Category)->getAllCategories();

		$uom = (new UnitOfMeasure)->getUnitofMeasures();

		return view('website.trader_details.item_details')->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('user_id', $user_id)->with('categories', $categories)->with('uom', $uom);
	}

	public function getDiscardedItemDetails() {

		$user_id = Auth::user()->id;

		$discarded_items = (new Items)->getDiscardedItems($user_id);

		foreach ($discarded_items as $ac) {

			$category_details = (new Items)->getCategoryDetails();

			$ac->category_details = $category_details;

		}

		$active_items = [];
		$expired_items = [];
		$all_orders = [];
		$active_orders = [];

		$categories = (new Category)->getAllCategories();

		$uom = (new UnitOfMeasure)->getUnitofMeasures();

		return view('website.trader_details.item_details')->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('user_id', $user_id)->with('categories', $categories)->with('uom', $uom);
	}

	public function getActivePODetails() {

		$user_id = Auth::user()->id;

		$active_orders = (new PurchaseOrder)->getPurchaseOrders([1, 2, 3], $user_id);

		foreach ($active_orders as $ac) {

			$item_details = (new PurchaseOrder)->getPODetails($ac->id, 0);

			$ac->item_details = $item_details;

		}

		$active_items = [];
		$expired_items = [];
		$all_orders = [];
		$discarded_items = [];

		$categories = (new Category)->getAllCategories();

		$uom = (new UnitOfMeasure)->getUnitofMeasures();

		return view('website.trader_details.item_details')->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('user_id', $user_id)->with('categories', $categories)->with('uom', $uom);
	}

	public function getAllPODetails() {

		$user_id = Auth::user()->id;

		$all_orders = (new PurchaseOrder)->getPurchaseOrders([4], $user_id);

		foreach ($all_orders as $ac) {

			$item_details = (new PurchaseOrder)->getPODetails($ac->id, 0);

			$ac->item_details = $item_details;

		}

		$active_items = [];
		$expired_items = [];
		$active_orders = [];
		$discarded_items = [];

		$categories = (new Category)->getAllCategories();

		$uom = (new UnitOfMeasure)->getUnitofMeasures();

		return view('website.trader_details.item_details')->with('active_items', $active_items)->with('expired_items', $expired_items)->with('discarded_items', $discarded_items)->with('all_orders', $all_orders)->with('active_orders', $active_orders)->with('user_id', $user_id)->with('categories', $categories)->with('uom', $uom);
	}

	public function getSubCategories(Request $request) {

		$sub_categories = (new SubCategory)->getAllSubCategories($request->category_id);

		return response()->json(array('sub_categories' => $sub_categories));
	}

	public function getParameters(Request $request) {

		$parameters = (new SubCategoryToParameterMapping)->getSubToParaMappingDetails($request->sub_category_id);

		return response()->json(array('parameters' => $parameters));
	}

	public function storeItemInfo(ItemRequest $request) {

		$user_id = Auth::user()->id;

		$request['unit_of_measure'] = $request->uom;
		$request['item_image'] = $request->item_image;
		$request['supplier_id'] = $user_id;

		(new Items())->store($request->all());

		return response(1);

	}

	public function updateItemInfo(ItemRequest $request) {

		(new Items())->reconsiderItem($request->item_id);

		$user_id = Auth::user()->id;

		$request['unit_of_measure'] = $request->uom;
		$request['item_image'] = $request->revise_item_image;
		$request['supplier_id'] = $user_id;
		$request['reference_id'] = $request->item_id;

		(new Items())->store($request->all());

		return response(1);

	}

}
