<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BankRequest extends Request {

	public function authorize() {
		return true;
	}

	public function rules() {

		if ($this->method() == 'PUT') {
			// Update operation, exclude the record with id from the validation:
			$bank_rule1 = 'required:bank_details,bank_name,' . $this->get('id');
			$bank_rule2 = 'required:bank_details,branch_email,' . $this->get('id');
			$bank_rule3 = 'required:bank_details,ifsc_code,' . $this->get('id');
			$bank_rule4 = 'required:bank_details,micr_code,' . $this->get('id');
			$bank_rule5 = 'required:bank_details,address,' . $this->get('id');
			$bank_rule6 = 'required:bank_details,account_holders_name,' . $this->get('id');
			$bank_rule7 = 'required:bank_details,account_no,' . $this->get('id');
			$bank_rule8 = 'required:bank_details,cancelled_cheque_attachment,' . $this->get('id');
			$bank_rule9 = 'required:bank_details,branch_name,' . $this->get('id');

		} else {
			// Create operation. There is no id yet.
			$bank_rule1 = 'required:bank_details,bank_name';
			$bank_rule2 = 'required|email:bank_details,branch_email';
			$bank_rule3 = 'required:bank_details,ifsc_code';
			$bank_rule4 = 'required:bank_details,micr_code';
			$bank_rule5 = 'required:bank_details,address';
			$bank_rule6 = 'required:bank_details,account_holders_name';
			$bank_rule7 = 'required:bank_details,account_no';
			$bank_rule8 = 'required:bank_details,cancelled_cheque_attachment';
			$bank_rule9 = 'required:bank_details,branch_name';

		}

		return [
			'bank_name' => $bank_rule1,
			'branch_email' => $bank_rule2,
			'ifsc_code' => $bank_rule3,
			'micr_code' => $bank_rule4,

			'address' => $bank_rule5,
			'account_holders_name' => $bank_rule6,
			'account_no' => $bank_rule7,
			'image_file' => $bank_rule8,
			'branch_name' => $bank_rule9,
			'account_type' => 'required|not_in:0',
			'city_id' => 'required|not_in:0',

		];
	}
}
