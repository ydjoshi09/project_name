<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SubCategoryToParameterMapping extends Model
{
    //
    use SoftDeletes;
	protected $table='sub_category_to_parameter_mappings';
	protected $dates = ['deleted_at'];
	protected $fillable = [
			'parameter_id',
            'sub_category_id',
            'mandatory_flag'
            
	];

    public function getAllSubToParaMapping()
    {
        return $this->where('sub_category_id',$id)->lists('sub_category_id', 'id');
    }

    public function getSubToParaMappingDetails($sub_category_id)
    {
   
    $parameters=DB::table('sub_category_to_parameter_mappings as scpm')
   ->join('parameters as p','p.id','=','scpm.parameter_id')
   ->where('scpm.sub_category_id',$sub_category_id)
   ->get();

   return $parameters;

    }
}
