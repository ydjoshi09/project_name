<?php

namespace App\Models\UserDetails;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ShippingAddress extends Model
{
    //Author : Sheevathsa
   	//Date: 6/4/2018

    // use SoftDeletes;
	protected $table='shipping_address';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'user_id',
		'city_id',
		'address',
		'active_flag',
	];

	// This will create a new Shipping address.
	public function store($request)
	{
		$shipping_address=$this->create($request);    
		return $shipping_address;
	}

	// This function will return details of Shipping address which has id as provided value.
	public function getShippingAddressById($id)
	{
		return $this->where('id',$id)->first();
	}

	// This function will store new values to entry.
	public function updateShippingAddressDetails($id,$request)
	{
		$shipping_address = new ShippingAddress();
		$shipping_address=$shipping_address->getShippingAddressById($id);
		$shipping_address->update($request);
		return 1;
	}

	// This function will set active_flag to 1, if any other entry which belongs to same user and has active flag 1,will be updated to active flag 0
	public function updateActiveShippingAddressDetails($id,$user_id)
	{
		$this->where('user_id',$user_id)->update(['active_flag'=>0]);
		$this->where('id',$id)->update(['active_flag'=>1]);
		return 1;
	}

	
	// This will return Active Shipping Address address of user.

	public function getActiveShippingAddressDetail($user_id)
	{
		return $this->where('user_id',$user_id)->where('active_flag',1)->first();
	}

	// This will return the list of Shipping address of user.
	public function getUserShippingAddressDetail($user_id)
	{
		// return $this->where('user_id',$user_id)->lists('address', 'id');

		

          $shipping_address=DB::table('shipping_address as b')
       ->join('cities AS c','c.id','=','b.city_id')
       ->join('states AS s','s.id','=','c.state_id')
       ->join('countries AS co','co.id','=','s.country_id')
       ->select('b.*','co.id as country_id','co.country_name','co.phone_code','s.id as state_id','s.state_name','c.id as city_id','c.city_name')
       ->where('b.user_id',$user_id)
       ->get();
   

         return $shipping_address;
	}



	
}
