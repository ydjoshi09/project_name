<?php

namespace App\Models\Masters;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cities extends Model {
	//
	use SoftDeletes;
	protected $table = 'cities';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'city_name',
		'state_id',
	];
	protected $with = array('state');

	public function state() {
		return $this->belongsTo('App\Models\Masters\States', 'state_id');

	}

	// This function will return specific cities.
	public function getSpecificCities($state_id) {

		return DB::table('cities')->where('state_id', $state_id)->get();

	}

}
