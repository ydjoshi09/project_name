<?php

namespace App\Models\Masters;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class States extends Model {
	//
	use SoftDeletes;
	protected $table = 'states';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'country_id',
		'state_name',
	];
	protected $with = array('country');

	public function country() {
		return $this->belongsTo('App\Models\Masters\Country', 'country_id');
	}

	// This function will return specific states.
	public function getSpecificStates($country_id) {

		return DB::table('states')->where('country_id', $country_id)->get();
	}

}
